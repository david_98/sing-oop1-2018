package alert;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import prozori.RegistrovanjeKorisnika;

public class AlertKreiranja {
	public static void popUp() {
		Stage window1 = new Stage();
		window1.initModality(Modality.APPLICATION_MODAL);
		window1.setTitle("Upozorenje!");
		
		VBox vbox = new VBox(10);
		vbox.setAlignment(Pos.CENTER);
		
		Label labela = new Label("Da li ste sigurni da zelite da prekinete unos?");
		
		HBox hbox = new HBox();
		hbox.setSpacing(7);
		Button btn1 = new Button("Da");
		btn1.setMinWidth(50);
		btn1.setOnAction(e -> {
			window1.close();
			RegistrovanjeKorisnika.broj = 0; // vracamo broj na 0, kako ne bi izazvali pop up prozor ukoliko administrator odluci da ponovo izabere registrovanje korisnika
			RegistrovanjeKorisnika.window.close();
		});
		Button btn2 = new Button("Ne");
		btn2.setMinWidth(50);
		btn2.setOnAction(e -> window1.close());
		hbox.getChildren().addAll(btn1, btn2);
		hbox.setAlignment(Pos.CENTER);
		
		btn1.getStyleClass().add("plavoDugme");
		btn2.getStyleClass().add("crvenoDugme");
		
		vbox.getChildren().addAll(labela, hbox);
		
		Scene scena = new Scene(vbox, 280, 150);
		scena.getStylesheets().add("css/registrovanjeKorisnika.css");
		window1.setScene(scena);
		window1.showAndWait();
	}
}
