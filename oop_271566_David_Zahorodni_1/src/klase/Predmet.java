package klase;

public class Predmet {
	private String id;
	private String naziv;
	private String silabus;
	private String smer;
	private int godina;
	private String godinaStudija;
	private int fondPredavanja;
	private int fondVezbi;
	
	public Predmet() {
		
	}
	public Predmet(String id, String naziv, String silabus, String smer, int godina, String godinaStudija, int fondPredavanja, int fondVezbi) {
		this.id = id;
		this.naziv = naziv;
		this.silabus = silabus;
		this.smer = smer;
		this.godina = godina;
		this.godinaStudija = godinaStudija;
		this.fondPredavanja = fondPredavanja;
		this.fondVezbi = fondVezbi;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getSilabus() {
		return silabus;
	}
	public void setSilabus(String silabus) {
		this.silabus = silabus;
	}
	
	public String getSmer() {
		return smer;
	}
	public void setSmer(String smer) {
		this.smer = smer;
	}
	public int getGodina() {
		return godina;
	}
	public void setGodina(int godina) {
		this.godina = godina;
	}
	public String getGodinaStudija() {
		return godinaStudija;
	}
	public void setGodinaStudija(String godinaStudija) {
		this.godinaStudija = godinaStudija;
	}
	public int getFondPredavanja() {
		return fondPredavanja;
	}
	public void setFondPredavanja(int fondPredavanja) {
		this.fondPredavanja = fondPredavanja;
	}
	public int getFondVezbi() {
		return fondVezbi;
	}
	public void setFondVezbi(int fondVezbi) {
		this.fondVezbi = fondVezbi;
	}
	
	
}
