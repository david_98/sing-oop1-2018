package klase;

import org.json.JSONObject;

public class Student extends Korisnik {
	private String email;
	private long brojTelefona;
	private JSONObject indeks = new JSONObject();
	
	
	public Student() {
		super();
	}
	public Student(String ime, String prezime, String korisnickoIme, String lozinka, boolean obrisan, String email, long brojTelefona, JSONObject indeks) {
		super(ime, prezime, korisnickoIme, lozinka, obrisan);
		this.email = email;
		this.brojTelefona = brojTelefona; 
		this.indeks = indeks;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getBrojTelefona() {
		return brojTelefona;
	}
	public void setBrojTelefona(long brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
	public JSONObject getIndeks() {
		return indeks;
	}
	public void setIndeks(JSONObject indeks) {
		this.indeks = indeks;
	}
	
}
