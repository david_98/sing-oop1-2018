package klase;

import enumi.Zvanje;

public class Nastavnik extends Korisnik {
	private String email;
	private Zvanje zvanje;
	
	public Nastavnik() {
		super();
		
	}
	public Nastavnik(String ime, String prezime, String korisnickoIme, String lozinka, boolean obrisan, String email, Zvanje zvanje) {
		super(ime, prezime, korisnickoIme, lozinka, obrisan);
		this.email = email;
		this.zvanje = zvanje;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Zvanje getZvanje() {
		return zvanje;
	}
	public void setZvanje(Zvanje zvanje) {
		this.zvanje = zvanje;
	}
	
	
}
