package enumi;

public enum Zvanje {
	SARADNIK,
	ASISTENT,
	DOCENT,
	VANREDNI_PROFESOR,
	REDOVAN_PROFESOR
}
