package prozori;

import java.io.*;
import org.json.*;

import enumi.Zvanje;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PromenaPodatakaOKorisniku {
		
	static Button sacuvaj;
	static Stage window;
	static Label labela;
	static HBox hbox;
	static JSONObject objekat = null;
	static JSONObject izmenjeniRecnik = null;
	static HBox hbox2;
	static Label labelaObavestenja;
	static Boolean stanjeKorisnika;
	
	public static void prikaz() {
		window = new Stage();
		window.setTitle("Promena podataka o korisniku");
		
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		labela = new Label("Cije podatke bi zeleli da promenite?");
		
		// vadimo json objekat iz korisnici.json
		FileReader citac = null;
        try {
        	citac = new FileReader("data/korisnici.json");
        	objekat = new JSONObject(new JSONTokener(citac));
        	citac.close();
        } catch (IOException | JSONException b) {
        	System.out.println("Doslo je do greske prilikom citanje iz korisnici.json.");
        }
        
		hbox = new HBox();
		hbox.setSpacing(10);
		hbox.setAlignment(Pos.CENTER);
		Button btn1 = new Button("Moje");
		btn1.setStyle("-fx-background-color: #AB4642; -fx-text-fill: #E8E8E8; -fx-font-weight: bold");
		btn1.setMinWidth(80);
		btn1.setOnAction(e ->{
			prikazGridAdministratora();
		});
		// pravimo button za svaki tip korisnika
		Button btn12 = new Button("Administratorove");
		btn12.setStyle("-fx-background-color: #A1B56C; -fx-text-fill: #E8E8E8; -fx-font-weight: bold");
		btn12.setMinWidth(80);
		btn12.setOnAction(e ->{
			pretragaAdministratora();
		});
		
		Button btn2 = new Button("Nastavnikove");
		btn2.setStyle("-fx-background-color: #86C1B9; -fx-text-fill: #E8E8E8; -fx-font-weight: bold");
		btn2.setMinWidth(80);
		btn2.setOnAction(e ->{
			pretragaNastavnika();
		});
		
		Button btn3 = new Button("Studentove");
		btn3.setStyle("-fx-background-color: #A16946; -fx-text-fill: #E8E8E8; -fx-font-weight: bold");
		btn3.setMinWidth(80);
		btn3.setOnAction(e ->{
			pretragaStudenta();
		});
		hbox.getChildren().addAll(btn1, btn12, btn2, btn3);
		vbox.getChildren().addAll(labela, hbox);
		
		hbox2 = new HBox();
		labelaObavestenja = new Label("Korisnicko ime je zauzeto, pokusajte sa drugim.");
		labelaObavestenja.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		labelaObavestenja.setVisible(false);
		
		// dugme "sacuvaj izmenu"
		sacuvaj = new Button("Sacuvaj izmene");
		sacuvaj.getStyleClass().add("potvrdnaDugmad");
		sacuvaj.setMinWidth(80);
		sacuvaj.setTranslateX(35);
		
		hbox2.getChildren().addAll(labelaObavestenja, sacuvaj);
		
		// scena
		Scene scena = new Scene(vbox, 450, 400);
		scena.getStylesheets().add("css/promenaPodatakaOKorisniku.css");
		window.setScene(scena);
		window.show();
	}
	private static void prikazGridAdministratora() {
		labelaObavestenja.setVisible(false);
		GridPane gridAdministratora = new GridPane();
		gridAdministratora.setAlignment(Pos.CENTER);
		gridAdministratora.setVgap(6);
		Label label1 = new Label("Promenite ime: ");
		TextField ime = new TextField();
		
		Label label2 = new Label("Promenite prezime: ");
		TextField prezime = new TextField();
		
		Label label3 = new Label("Promenite korisnicko ime: ");
		TextField korisnickoIme = new TextField();
		
		Label label4 = new Label("Promenite lozinku: ");
		TextField lozinka = new TextField();
	
		Label obavestenjeOPoljima = new Label("Sva polja moraju biti popunjena.");
		obavestenjeOPoljima.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeOPoljima.setVisible(false);
		
		gridAdministratora.add(label1, 0, 0);
		gridAdministratora.add(ime, 1, 0);
		gridAdministratora.add(label2, 0, 1);
		gridAdministratora.add(prezime, 1, 1);
		gridAdministratora.add(label3, 0, 2);
		gridAdministratora.add(korisnickoIme, 1, 2);
		gridAdministratora.add(label4, 0, 3);
		gridAdministratora.add(lozinka, 1, 3);
		gridAdministratora.add(obavestenjeOPoljima, 0, 4);

		JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
		for (Object recnik: listaAdministratora) {
			JSONObject recnik2 = (JSONObject) recnik;
			if (PrijavljivanjeNaSistem.trenutniKorisnik.getString("korisnickoIme").equals(recnik2.getString("korisnickoIme"))) {
				izmenjeniRecnik = recnik2;  //PODSETNIK MENI: stavio u staticko polje izmenjeniRecnik kako bi ga mogao koristiti i van for petlje
				                            //PODSETNIK MENI: izvrsio sam pretragu da nadjem json objekat iz statickog JSONObject objekta kako bi nakon izvrsene izmene na tom njegovom manjem JSONObjektu (izmenjeniRecnik) prilikom upisa statickog jsonobjekta u json fajl upisao i izmene
				ime.setText(recnik2.getString("ime"));
				prezime.setText(recnik2.getString("prezime"));
				korisnickoIme.setText(recnik2.getString("korisnickoIme"));
				lozinka.setText(recnik2.getString("lozinka"));
				break;
			}
		}
		sacuvaj.setOnAction(e -> {
			obavestenjeOPoljima.setVisible(false);
			boolean pronadjen = false;
        	if (ime.getText().equals("") || prezime.getText().equals("") || korisnickoIme.getText().equals("") || lozinka.getText().equals("")) {
        		obavestenjeOPoljima.setVisible(true);
        		return;
        	}
			JSONArray listaSvihAdministratora = (JSONArray) objekat.get("administratori");
			for (Object recnikk: listaSvihAdministratora) { //OBAVESTENJE MENI: ne mozes staviti recnik ovde (gde je sad recnikk) jer vec imas promenjivu recnik tj. ubacio si je u funkciju preko preko argumenta prilikom poziva ove funkcije
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			// radim slicno jos dva puta samo sad da proverim kod nastavnika i kod studenata
			JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
			for (Object recnikk: listaStudenata) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
			for (Object recnikk: listaNastavnika) {
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			// ovde se gleda da li je tekst koji se nalazi na polju korisnickoIme mozda isti onaj koji se nalazi pod kljucem korisnickoIme recnika prosledjenog kao argument ovoj funkciji (recnik osobe cije podatke menjamo)
			if (izmenjeniRecnik.getString("korisnickoIme").equals(korisnickoIme.getText())) {
				pronadjen = false;
			}
			if (pronadjen) {
				labelaObavestenja.setVisible(true);
				korisnickoIme.clear();
				korisnickoIme.requestFocus();
			}
        	if (!pronadjen) {
        		izmenjeniRecnik.put("ime", ime.getText());
    			izmenjeniRecnik.put("prezime", prezime.getText());
    			izmenjeniRecnik.put("korisnickoIme", korisnickoIme.getText());
    			izmenjeniRecnik.put("lozinka", lozinka.getText());
    			izmenjeniRecnik.put("obrisan", false);
    			PrintWriter upis  = null;
    			try {
    				upis = new PrintWriter("data/korisnici.json");
    				upis.write(objekat.toString(4));
    				upis.close();
    			} catch (IOException | JSONException b) {
    				System.out.println("Greska prilikom upisa u json fajl.");
    			}
    			window.close();
        	}
			
			
		});
		
		
		
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		vbox.getChildren().addAll(labela, hbox, gridAdministratora, hbox2);
		Scene scena = new Scene(vbox, 450, 400);
		scena.getStylesheets().add("css/promenaPodatakaOKorisniku.css");
		window.setScene(scena);
		window.show();
	}
	//funkcije za pretrage
	
	private static void pretragaAdministratora() {
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		HBox hbox1 = new HBox();
		hbox1.setSpacing(10);
		hbox1.setAlignment(Pos.CENTER);
		TextField unos = new TextField();
		unos.setPromptText("Unesite korisnicko ime administratora");
		unos.setStyle("-fx-prompt-text-fill: #282828");
		Button proveri = new Button("Dalje");
		proveri.getStyleClass().add("potvrdnaDugmad");
		Label obavestenje = new Label("Nema poklapanja, pokusajte ponovo.");
		obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
	    obavestenje.setVisible(false);
		proveri.setOnAction(e -> {
			boolean pronadjen = false;
			JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
			for (Object recnik: listaAdministratora) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(unos.getText())) {
					pronadjen = true;
					prikazGridDrugihAdministratora(recnik2);
				}
			}
			if (!pronadjen) {
				obavestenje.setVisible(true);
				unos.clear();
				unos.requestFocus();
			}
			
		});
		
		unos.setMinWidth(215);
		proveri.setMinWidth(70);
		hbox1.getChildren().addAll(unos, proveri);
		
		vbox.getChildren().addAll(labela, hbox, hbox1, obavestenje);
		Scene scena = new Scene(vbox, 450, 400);
		scena.getStylesheets().add("css/promenaPodatakaOKorisniku.css");
		window.setScene(scena);
		window.show();
	
	}
	private static void pretragaNastavnika() {
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		HBox hbox1 = new HBox();
		hbox1.setSpacing(10);
		hbox1.setAlignment(Pos.CENTER);
		TextField unos = new TextField();
		unos.setPromptText("Unesite korisnicko ime nastavnika");
		unos.setStyle("-fx-prompt-text-fill: #282828");
		Button proveri = new Button("Dalje");
		proveri.getStyleClass().add("potvrdnaDugmad");
		Label obavestenje = new Label("Nema poklapanja, pokusajte ponovo.");
		obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
	    obavestenje.setVisible(false);
		proveri.setOnAction(e -> {
			boolean pronadjen = false;
			JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
			for (Object recnik: listaNastavnika) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(unos.getText())) {
					pronadjen = true;
					prikazGridNastavnika(recnik2);
				}
			}
			if (!pronadjen) {
				obavestenje.setVisible(true);
				unos.clear();
				unos.requestFocus();
			}
			
		});
		
		unos.setMinWidth(195);
		proveri.setMinWidth(70);
		hbox1.getChildren().addAll(unos, proveri);
		
		vbox.getChildren().addAll(labela, hbox, hbox1, obavestenje);
		Scene scena = new Scene(vbox, 450, 400);
		scena.getStylesheets().add("css/promenaPodatakaOKorisniku.css");
		window.setScene(scena);
		window.show();
	}
	
	private static void pretragaStudenta() {
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		HBox hbox1 = new HBox();
		hbox1.setSpacing(10);
		hbox1.setAlignment(Pos.CENTER);
		TextField unos = new TextField();
		unos.setPromptText("Unesite korisnicko ime studenta");
		unos.setStyle("-fx-prompt-text-fill: #282828");
		Button proveri = new Button("Dalje");
		proveri.getStyleClass().add("potvrdnaDugmad");
		Label obavestenje = new Label("Nema poklapanja, pokusajte ponovo.");
		obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
	    obavestenje.setVisible(false);
		proveri.setOnAction(e -> {
			boolean pronadjen = false;
			JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
			for (Object recnik: listaStudenata) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(unos.getText())) {
					pronadjen = true;
					prikazGridStudenta(recnik2);
				}
			}
			if (!pronadjen) {
				obavestenje.setVisible(true);
				unos.clear();
				unos.requestFocus();
			}
			
		});
		
		unos.setMinWidth(195);
		proveri.setMinWidth(70);
		hbox1.getChildren().addAll(unos, proveri);
		
		vbox.getChildren().addAll(labela, hbox, hbox1, obavestenje);
		Scene scena = new Scene(vbox, 450, 400);
		scena.getStylesheets().add("css/promenaPodatakaOKorisniku.css");
		window.setScene(scena);
		window.show();
	}
	
	// funkcije za izmene
	private static void prikazGridDrugihAdministratora(JSONObject recnik) {
		labelaObavestenja.setVisible(false);
		GridPane gridAdministratora = new GridPane();
		gridAdministratora.setAlignment(Pos.CENTER);
		gridAdministratora.setVgap(6);
		Label label1 = new Label("Promenite ime administratora: ");
		TextField ime = new TextField();
		
		Label label2 = new Label("Promenite prezime administratora: ");
		TextField prezime = new TextField();
		
		Label label3 = new Label("Promenite korisnicko ime administratora: ");
		TextField korisnickoIme = new TextField();
		
		Label label4 = new Label("Promenite lozinku administratora: ");
		TextField lozinka = new TextField();
		
		String str = "";
		if (recnik.getBoolean("obrisan") == true) {
			str = "je obrisan";
		} else {
			str = "nije obrisan";
		}
		stanjeKorisnika = recnik.getBoolean("obrisan");
		
		Label labela5 = new Label("Ovaj korisnik " + str);
		Button btn = new Button("promeni");
		btn.setOnAction(e -> {
			if (recnik.getBoolean("obrisan") == true) {
				stanjeKorisnika = false;
			} else {
				stanjeKorisnika = true;
			}
			btn.setStyle("-fx-background-color: #AB4642");
			
		});
		
		Label obavestenjeOPoljima = new Label("Sva polja moraju biti popunjena.");
		obavestenjeOPoljima.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeOPoljima.setVisible(false);
		
		gridAdministratora.add(label1, 0, 0);
		gridAdministratora.add(ime, 1, 0);
		gridAdministratora.add(label2, 0, 1);
		gridAdministratora.add(prezime, 1, 1);
		gridAdministratora.add(label3, 0, 2);
		gridAdministratora.add(korisnickoIme, 1, 2);
		gridAdministratora.add(label4, 0, 3);
		gridAdministratora.add(lozinka, 1, 3);
		gridAdministratora.add(labela5, 0, 4);
		gridAdministratora.add(btn, 1, 4);
		gridAdministratora.add(obavestenjeOPoljima, 0, 5);

		
        ime.setText(recnik.getString("ime"));
        prezime.setText(recnik.getString("prezime"));
        korisnickoIme.setText(recnik.getString("korisnickoIme"));
        lozinka.setText(recnik.getString("lozinka"));
        
        sacuvaj.setOnAction(e -> {
        	obavestenjeOPoljima.setVisible(false);
        	labelaObavestenja.setVisible(false);
        	boolean pronadjen = false;
        	if (ime.getText().equals("") || prezime.getText().equals("") || korisnickoIme.getText().equals("") || lozinka.getText().equals("")) {
        		obavestenjeOPoljima.setVisible(true);
        		return;
        	}
			JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
			for (Object recnikk: listaAdministratora) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			// radim slicno jos dva puta samo sad da proverim kod nastavnika i kod studenata
			JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
			for (Object recnikk: listaNastavnika) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
			for (Object recnikk: listaStudenata) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			// ovde se gleda da li je tekst koji se nalazi na polju korisnickoIme mozda isti onaj koji se nalazi pod kljucem korisnickoIme recnika prosledjenog kao argument ovoj funkciji (recnik osobe cije podatke menjamo)
			if (recnik.getString("korisnickoIme").equals(korisnickoIme.getText())) {
				pronadjen = false;
			}
			if (pronadjen) {
				labelaObavestenja.setVisible(true);
				korisnickoIme.clear();
				korisnickoIme.requestFocus();
			}
        	if (!pronadjen) {
        		recnik.put("ime", ime.getText());
            	recnik.put("prezime", prezime.getText());
            	recnik.put("korisnickoIme", korisnickoIme.getText());
            	recnik.put("lozinka", lozinka.getText());
            	recnik.put("obrisan", stanjeKorisnika);
            	
            	PrintWriter upis = null;
            	try {
            		upis = new PrintWriter("data/korisnici.json");
            		upis.write(objekat.toString(4));
            		upis.close();
            	}catch(IOException | JSONException b) {
            		System.out.println("Greska prilikom upisa u json fajl.");
            	}
            	window.close();
        	}
     
        });
		
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		vbox.getChildren().addAll(labela, hbox, gridAdministratora, hbox2);
		Scene scena = new Scene(vbox, 450, 400);
		scena.getStylesheets().add("css/promenaPodatakaOKorisniku.css");
		window.setScene(scena);
		window.show();
	}
	
	private static void prikazGridNastavnika(JSONObject recnik) {
		labelaObavestenja.setVisible(false);
		GridPane gridNastavnika = new GridPane();
		gridNastavnika.setAlignment(Pos.CENTER);
		gridNastavnika.setVgap(6);
		Label label1 = new Label("Promenite ime nastavnika: ");
		TextField ime = new TextField();
		
		Label label2 = new Label("Promenite prezime nastavnika: ");
		TextField prezime = new TextField();
		
		Label label3 = new Label("Promenite korisnicko ime nastavnika: ");
		TextField korisnickoIme = new TextField();
		
		Label label4 = new Label("Promenite lozinku nastavnika: ");
		TextField lozinka = new TextField();
		
		Label label5 = new Label("Promenite email nastavnika: ");
		TextField email = new TextField();
		
		Label label6 = new Label("Promenite zvanje nastavnika: ");
		ChoiceBox<Zvanje> zvanje = new ChoiceBox<>();
		zvanje.getItems().addAll(
				Zvanje.REDOVAN_PROFESOR,
				Zvanje.VANREDNI_PROFESOR,
				Zvanje.ASISTENT,
				Zvanje.DOCENT,
				Zvanje.SARADNIK
				);
		zvanje.setValue(Zvanje.valueOf(String.valueOf(recnik.get("zvanje"))));
		
		Label obavestenjeOPoljima = new Label("Sva polja moraju biti popunjena.");
		obavestenjeOPoljima.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeOPoljima.setVisible(false);
		
		String str = "";
		if (recnik.getBoolean("obrisan") == true) {
			str = "je obrisan";
		} else {
			str = "nije obrisan";
		}
		stanjeKorisnika = recnik.getBoolean("obrisan");
		
		Label labela7 = new Label("Ovaj korisnik " + str);
		Button btn = new Button("promeni");
		btn.setOnAction(e -> {
			if (recnik.getBoolean("obrisan") == true) {
				stanjeKorisnika = false;
			} else {
				stanjeKorisnika = true;
			}
			btn.setStyle("-fx-background-color: #AB4642");
			
		});
		
		gridNastavnika.add(label1, 0, 0);
		gridNastavnika.add(ime, 1, 0);
		gridNastavnika.add(label2, 0, 1);
		gridNastavnika.add(prezime, 1, 1);
		gridNastavnika.add(label3, 0, 2);
		gridNastavnika.add(korisnickoIme, 1, 2);
		gridNastavnika.add(label4, 0, 3);
		gridNastavnika.add(lozinka, 1, 3);
		gridNastavnika.add(label5, 0, 4);
		gridNastavnika.add(email, 1, 4);
		gridNastavnika.add(label6, 0, 5);
		gridNastavnika.add(zvanje, 1, 5);
		gridNastavnika.add(labela7, 0, 6);
		gridNastavnika.add(btn, 1, 6);
		gridNastavnika.add(obavestenjeOPoljima, 0, 7);
		
        ime.setText(recnik.getString("ime"));
        prezime.setText(recnik.getString("prezime"));
        korisnickoIme.setText(recnik.getString("korisnickoIme"));
        lozinka.setText(recnik.getString("lozinka"));
        email.setText(recnik.getString("email"));
        
        sacuvaj.setOnAction(e -> {
        	obavestenjeOPoljima.setVisible(false);
        	labelaObavestenja.setVisible(false);
        	boolean pronadjen = false;
        	if (ime.getText().equals("") || prezime.getText().equals("") || korisnickoIme.getText().equals("") || lozinka.getText().equals("")) {
        		obavestenjeOPoljima.setVisible(true);
        		return;
        	}
			JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
			for (Object recnikk: listaNastavnika) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			// radim slicno jos dva puta samo sad da proverim kod administratora i kod studenata
			JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
			for (Object recnikk: listaAdministratora) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
			for (Object recnikk: listaStudenata) {
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			// ovde se gleda da li je tekst koji se nalazi na polju korisnickoIme mozda isti onaj koji se nalazi pod kljucem korisnickoIme recnika prosledjenog kao argument ovoj funkciji (recnik osobe cije podatke menjamo)
			if (recnik.getString("korisnickoIme").equals(korisnickoIme.getText())) {
				pronadjen = false;
			}
			if (pronadjen) {
				labelaObavestenja.setVisible(true);
				korisnickoIme.clear();
				korisnickoIme.requestFocus();
			}
        	if (!pronadjen) {
        		recnik.put("ime", ime.getText());
            	recnik.put("prezime", prezime.getText());
            	recnik.put("korisnickoIme", korisnickoIme.getText());
            	recnik.put("lozinka", lozinka.getText());
            	recnik.put("email", email.getText());
            	recnik.put("zvanje", zvanje.getValue());
            	recnik.put("obrisan", stanjeKorisnika);
            	
            	PrintWriter upis = null;
            	try {
            		upis = new PrintWriter("data/korisnici.json");
            		upis.write(objekat.toString(4));
            		upis.close();
            	}catch(IOException | JSONException b) {
            		System.out.println("Greska prilikom upisa u json fajl.");
            	}
            	window.close();
        	}
        	
        });
		
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		vbox.getChildren().addAll(labela, hbox, gridNastavnika, hbox2);
		Scene scena = new Scene(vbox, 450, 400);
		scena.getStylesheets().add("css/promenaPodatakaOKorisniku.css");
		window.setScene(scena);
		window.show();
	}
	
	private static void prikazGridStudenta(JSONObject recnik) {
		labelaObavestenja.setVisible(false);
		GridPane gridStudenta = new GridPane();
		gridStudenta.setAlignment(Pos.CENTER);
		gridStudenta.setVgap(6);
		Label label1 = new Label("Promenite ime studenta: ");
		TextField ime = new TextField();
		
		Label label2 = new Label("Promenite prezime studenta: ");
		TextField prezime = new TextField();
		
		Label label3 = new Label("Promenite korisnicko ime studenta: ");
		TextField korisnickoIme = new TextField();
		
		Label label4 = new Label("Promenite lozinku studenta: ");
		TextField lozinka = new TextField();
		
		Label label5 = new Label("Promenite email studenta: ");
		TextField email = new TextField();
		
		Label label6 = new Label("Promenite broj telefona studenta: ");
		TextField brojTelefona = new TextField();
		
		Label label7 = new Label("Izmena indeksa studenta: ");
		
		// za indeks
		Label label8 = new Label("Promenite broj indeksa studenta: "); 
		TextField indeksBroj = new TextField();
		
		Label label9 = new Label("Promenite univerzitet studenta: ");
		TextField indeksUniverzitet = new TextField();
		
		Label label10 = new Label("Promenite fakultet studenta: ");
		TextField indeksFakultet = new TextField();
		
		Label label11 = new Label("Promenite smer studenta: ");
		TextField indeksSmer = new TextField();
		
		Label label12 = new Label("Promenite godinu upisa studenta: ");
		TextField indeksGodinaUpisa = new TextField();
		
		Label label13 = new Label("Promenite godinu studija studenta: ");
		TextField indeksGodinaStudija = new TextField();
		
		Label label15 = new Label("Promenite ime, prezime studenta: ");
		Label label16 = new Label("(izmena se vrsi promenom polja ime i prezime navedenih gore)");
		TextField indeksStudent = new TextField();
	    indeksStudent.setEditable(false);
		 
		
		Label obavestenjeOPoljima = new Label("Sva polja moraju biti popunjena.");
		obavestenjeOPoljima.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeOPoljima.setVisible(false);
		
		Label obavestenjeOTelefonu = new Label("Broj telefona ne moze da sadrzi slova.");
		obavestenjeOTelefonu.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeOTelefonu.setVisible(false);
		
		Label obavestenjeOBrojevima = new Label("Godina upisa i broj indeksa  ne mogu sadrzati slova.");
		obavestenjeOBrojevima.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeOBrojevima.setVisible(false);
		
		Label obavestenjeOIndeksu = new Label("Broj indeksa je zauzet, pokusajte sa drugim.");
		obavestenjeOIndeksu.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeOIndeksu.setVisible(false);
		
		Label obavestenjeOGodini = new Label("Godina studija moze biti: prva, druga, treca ili cetvrta.");
		obavestenjeOGodini.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeOGodini.setVisible(false);
		
		// kreiram string koji cu dodati u labelu kako bi administrator video da li je korisnik koga menja obrisan ili ne
		String str = "";
		if (recnik.getBoolean("obrisan") == true) {
			str = "je obrisan";
		} else {
			str = "nije obrisan";
		}
		stanjeKorisnika = recnik.getBoolean("obrisan");
		
		Label labela8 = new Label("Ovaj korisnik " + str);
		Button btn = new Button("promeni");
		btn.setOnAction(e -> {
			if (recnik.getBoolean("obrisan") == true) {
				stanjeKorisnika = false;
			} else {
				stanjeKorisnika = true;
			}
			btn.setStyle("-fx-background-color: #AB4642");
			
		});
		
		gridStudenta.add(label1, 0, 0);
		gridStudenta.add(ime, 1, 0);
		gridStudenta.add(label2, 0, 1);
		gridStudenta.add(prezime, 1, 1);
		gridStudenta.add(label3, 0, 2);
		gridStudenta.add(korisnickoIme, 1, 2);
		gridStudenta.add(label4, 0, 3);
		gridStudenta.add(lozinka, 1, 3);
		gridStudenta.add(label5, 0, 4); 
		gridStudenta.add(email, 1, 4);
		gridStudenta.add(label6, 0, 5);
		gridStudenta.add(brojTelefona, 1, 5);
		gridStudenta.add(label7, 0, 6);

		gridStudenta.add(label8, 0, 7);
		gridStudenta.add(indeksBroj, 1, 7);
		gridStudenta.add(label9, 0, 8);
		gridStudenta.add(indeksUniverzitet, 1, 8);
		gridStudenta.add(label10, 0, 9);
		gridStudenta.add(indeksFakultet, 1, 9);
		gridStudenta.add(label11, 0, 10);
		gridStudenta.add(indeksSmer, 1, 10);
		gridStudenta.add(label12, 0, 11);
		gridStudenta.add(indeksGodinaUpisa, 1, 11);
		gridStudenta.add(label13, 0, 12);
		gridStudenta.add(indeksGodinaStudija, 1, 12);
		
		gridStudenta.add(label15, 0, 13);
		gridStudenta.add(indeksStudent, 1, 13);
		gridStudenta.add(label16, 0, 14);

		gridStudenta.add(labela8, 0, 15);
		gridStudenta.add(btn, 1, 15);
		gridStudenta.add(hbox2, 1, 16);
		gridStudenta.add(obavestenjeOPoljima, 0, 17);
		gridStudenta.add(obavestenjeOTelefonu, 0, 18);
		gridStudenta.add(obavestenjeOBrojevima, 0, 19);
		gridStudenta.add(obavestenjeOIndeksu, 0, 20);
		gridStudenta.add(obavestenjeOGodini, 0, 21);
		
		
		ime.setText(recnik.getString("ime"));
        prezime.setText(recnik.getString("prezime"));
        korisnickoIme.setText(recnik.getString("korisnickoIme"));
        lozinka.setText(recnik.getString("lozinka"));
        email.setText(recnik.getString("email"));
        brojTelefona.setText(String.valueOf(recnik.getLong("brojTelefona")));
        indeksBroj.setText(String.valueOf(((JSONObject)recnik.get("indeks")).getInt("broj")));
        indeksUniverzitet.setText(((JSONObject)recnik.get("indeks")).getString("univerzitet"));
        indeksFakultet.setText(((JSONObject)recnik.get("indeks")).getString("fakultet"));
        indeksSmer.setText(((JSONObject)recnik.get("indeks")).getString("smer"));
        indeksGodinaUpisa.setText(String.valueOf(((JSONObject)recnik.get("indeks")).getInt("godina upisa")));
        indeksGodinaStudija.setText(((JSONObject)recnik.get("indeks")).getString("godina studija"));

        indeksStudent.setText(((JSONObject)recnik.get("indeks")).getString("student"));
        
        
        sacuvaj.setOnAction(e -> {
        	labelaObavestenja.setVisible(false);
        	obavestenjeOPoljima.setVisible(false);
        	obavestenjeOTelefonu.setVisible(false);
        	obavestenjeOBrojevima.setVisible(false);
        	obavestenjeOIndeksu.setVisible(false);
        	obavestenjeOGodini.setVisible(false);
        	
        	boolean pronadjen = false;
        	if (ime.getText().equals("") || prezime.getText().equals("") || korisnickoIme.getText().equals("") || lozinka.getText().equals("")) {
        		obavestenjeOPoljima.setVisible(true);
        		return;
        	}
        	if (email.getText().equals("") || brojTelefona.getText().equals("") || indeksBroj.getText().equals("") || indeksUniverzitet.getText().equals("")) {
        		obavestenjeOPoljima.setVisible(true);
        		return;
        	}
        	if (indeksFakultet.getText().equals("") || indeksSmer.getText().equals("") || indeksGodinaUpisa.getText().equals("") || indeksGodinaStudija.getText().equals("")) {
        		obavestenjeOPoljima.setVisible(true);
        		return;
        	}
        	if (!indeksGodinaStudija.getText().equals("prva") && !indeksGodinaStudija.getText().equals("druga") && !indeksGodinaStudija.getText().equals("treca") && !indeksGodinaStudija.getText().equals("cetvrta")) {
        		obavestenjeOGodini.setVisible(true);
        		return;
        	}
        	if (!brojTelefona.getText().matches("[0-9]+")) {
        		obavestenjeOTelefonu.setVisible(true);
        		return;
        	}
        	if (!indeksBroj.getText().matches("[0-9]+") || !indeksGodinaUpisa.getText().matches("[0-9]+")) {
				obavestenjeOBrojevima.setVisible(true);
				return;
			}
            // provera da li je korisnicko ime slobodno
			JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
			for (Object recnikk: listaStudenata) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			// radim slicno jos dva puta samo sad da proverim kod administratora i kod nastavnika
			JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
			for (Object recnikk: listaAdministratora) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
			for (Object recnikk: listaNastavnika) { 
				JSONObject recnik2 = (JSONObject) recnikk;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
				}
			}
			// ovde se gleda da li je tekst koji se nalazi na polju korisnickoIme mozda isti onaj koji se nalazi pod kljucem korisnickoIme recnika prosledjenog kao argument ovoj funkciji (recnik osobe cije podatke menjamo)
			if (recnik.getString("korisnickoIme").equals(korisnickoIme.getText())) {
				pronadjen = false;
			}
			
			//proveravam da li je novi indeks slobodan
			boolean uslovBroja = false;
			for (Object recnikk: listaStudenata) {
				JSONObject recnik2 = (JSONObject) recnikk;
				if (String.valueOf(((JSONObject)recnik2.get("indeks")).getInt("broj")).equals(indeksBroj.getText())) {
					uslovBroja = true;
				}
			}
			if (String.valueOf(((JSONObject)recnik.get("indeks")).getInt("broj")).equals(indeksBroj.getText())) {
				uslovBroja = false;
			}
			if (uslovBroja) {
				obavestenjeOIndeksu.setVisible(true);
				indeksBroj.clear();
				indeksBroj.requestFocus();
			}
			if (pronadjen) {
				labelaObavestenja.setVisible(true);
				korisnickoIme.clear();
				korisnickoIme.requestFocus();
			}
        	if (!pronadjen && !uslovBroja) {
        		recnik.put("ime", ime.getText());
            	recnik.put("prezime", prezime.getText());
            	recnik.put("korisnickoIme", korisnickoIme.getText());
            	recnik.put("lozinka", lozinka.getText());
            	recnik.put("email", email.getText());
            	recnik.put("brojTelefona", Long.parseLong(brojTelefona.getText()));
            	// popunjavanje HashMape
                JSONObject mapa = new JSONObject();
            	mapa.put("broj", Integer.parseInt(indeksBroj.getText()));
            	mapa.put("univerzitet", indeksUniverzitet.getText());
            	mapa.put("fakultet", indeksFakultet.getText());
            	mapa.put("smer", indeksSmer.getText());
            	mapa.put("godina upisa", Integer.parseInt(indeksGodinaUpisa.getText()));
            	mapa.put("godina studija", indeksGodinaStudija.getText());
            	
            	mapa.put("ispiti", ((JSONObject)recnik.get("indeks")).get("ispiti"));
            	// formiram string za kljuc "student" od vrednosti polja ime i prezime
            	String studentPodatak = ime.getText() + " " + prezime.getText();
            	mapa.put("student", studentPodatak);
            	recnik.put("indeks", mapa);
            	recnik.put("obrisan", stanjeKorisnika);
            	
            	PrintWriter upis = null;
            	try {
            		upis = new PrintWriter("data/korisnici.json");
            		upis.write(objekat.toString(4));
            		upis.close();
            	}catch(IOException | JSONException b) {
            		System.out.println("Greska prilikom upisa u json fajl.");
            	}
            	window.close();
        	}
        	
        	
        });
		
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		vbox.getChildren().addAll(labela, hbox, gridStudenta);
		Scene scena = new Scene(vbox, 850, 720);
		scena.getStylesheets().add("css/promenaPodatakaOKorisniku.css");
		window.setScene(scena);
		window.show(); // napomena meni: moze i bez jer je staticki
	}
	
	
}
