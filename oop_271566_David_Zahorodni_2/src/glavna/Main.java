package glavna;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Application;
import javafx.stage.Stage;
import klase.Administrator;
import klase.Nastavnik;
import klase.Student;
import prozori.PrijavljivanjeNaSistem;
import enumi.Zvanje;

public class Main extends Application {
		
	public static void main(String[] args) {
		// kreiranje pocetnih korisnika
		Administrator administrator1 = new Administrator("Aleksandra", "Mitrovic", "aMitrovic", "lozinkA", false);
		Nastavnik nastavnik1 = new Nastavnik("Nenad", "Peric", "nenadP", "lozNenad", false, "nenad.peric@gmail.com", Zvanje.REDOVAN_PROFESOR);
		
		JSONObject indeks = new JSONObject();
		indeks.put("broj", 2525);
		indeks.put("univerzitet", "Singidunum");
		indeks.put("fakultet", "Fakultet tehnickih nauka");
		indeks.put("smer", "Softversko i informaciono inzenjerstvo");
		indeks.put("godina upisa", 2016);
		indeks.put("godina studija", "druga");
		indeks.put("ispiti", new JSONArray());
		indeks.put("student", "Nikola Jovic");
		Student student1 = new Student("Nikola", "Jovic", "nikolaJ", "sirfaje", false, "nikola.jovic@gmai.com", 0655665256, indeks);
		
		//provera da li je falj vec kreiran i upis ukoliko nije
		File fajl = new File("data/korisnici.json");
		if (!fajl.exists()) {
			JSONObject korisniciObjekat = new JSONObject();
			korisniciObjekat.put("administratori", new JSONArray().put(new JSONObject(administrator1)));
			korisniciObjekat.put("nastavnici", new JSONArray().put(new JSONObject(nastavnik1)));
			korisniciObjekat.put("studenti", new JSONArray().put(new JSONObject(student1)));
			
			PrintWriter pr = null;
			
			try {
				pr = new PrintWriter("data/korisnici.json");
				pr.write(korisniciObjekat.toString(4));
				
			} catch(IOException | JSONException e) {
				System.out.println("Doslo je do greske prilikom upisa u korisnici.json.");
			} finally {
				if (pr != null) {  
					pr.close();
				}
			}
		}
		
		launch(args);
		
	}

	@Override
	public void start(Stage arg0) throws Exception {
		PrijavljivanjeNaSistem.prikaz();
	} 

}
