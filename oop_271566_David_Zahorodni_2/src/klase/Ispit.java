package klase;

import java.time.LocalDate;
import enumi.IspitniRok;

public class Ispit {
	private int indeks;
	private int bodovi;
	private int ocena;
	private LocalDate datumPolaganja;
	private boolean ponisten;
	private String predmet;
	private IspitniRok rok;
	
	public Ispit() {
		
	}
	public Ispit(int indeks, int bodovi, int ocena, LocalDate datumPolaganja, boolean ponisten, String predmet, IspitniRok rok) {
	this.indeks = indeks;
	this.bodovi = bodovi;
	this.ocena = ocena;
	this.datumPolaganja = datumPolaganja;
	this.ponisten = ponisten;
	this.predmet = predmet;
	this.rok = rok;
	}
	public int getIndeks() {
		return indeks;
	}
	public void setIndeks(int indeks) {
		this.indeks = indeks;
	}
	public int getBodovi() {
		return bodovi;
	}
	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}
	public int getOcena() {
		return ocena;
	}
	public void setOcena(int ocena) {
		this.ocena = ocena;
	}
	public LocalDate getDatumPolaganja() {
		return datumPolaganja;
	}
	public void setDatumPolaganja(LocalDate datumPolaganja) {
		this.datumPolaganja = datumPolaganja;
	}
	public boolean isPonisten() {
		return ponisten;
	}
	public void setPonisten(boolean ponisten) {
		this.ponisten = ponisten;
	}
	public String getPredmet() {
		return predmet;
	}
	public void setPredmet(String predmet) {
		this.predmet = predmet;
	}
	public IspitniRok getRok() {
		return rok;
	}
	public void setRok(IspitniRok rok) {
		this.rok = rok;
	}
	
	
}
