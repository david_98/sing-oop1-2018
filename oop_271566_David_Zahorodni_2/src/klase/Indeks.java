package klase;

public class Indeks {
	private int broj;
	private String univerzitet;
	private String fakultet;
	private String smer;
	private int godinaUpisa;
	private String godinaStudija;
	private Ispit[] listaIspita;
	private Student student;
	
	public Indeks() {
		
	}
	public Indeks(int broj, 
			String univerzitet, 
			String fakultet, 
			String smer, 
			int godinaUpisa, 
			String godinaStudija, 
			Ispit[] listaIspita, 
			Student student) {
		this.broj = broj;
		this.univerzitet = univerzitet;
		this.fakultet = fakultet;
		this.smer = smer;
		this.godinaUpisa = godinaUpisa;
		this.godinaStudija = godinaStudija;
		this.listaIspita = listaIspita;
		this.student = student;
	}
	public int getBroj() {
		return broj;
	}
	public void setBroj(int broj) {
		this.broj = broj;
	}
	public String getUniverzitet() {
		return univerzitet;
	}
	public void setUniverzitet(String univerzitet) {
		this.univerzitet = univerzitet;
	}
	public String getFakultet() {
		return fakultet;
	}
	public void setFakultet(String fakultet) {
		this.fakultet = fakultet;
	}
	public String getSmer() {
		return smer;
	}
	public void setSmer(String smer) {
		this.smer = smer;
	}
	public int getGodinaUpisa() {
		return godinaUpisa;
	}
	public void setGodinaUpisa(int godinaUpisa) {
		this.godinaUpisa = godinaUpisa;
	}
	public String getGodinaStudija() {
		return godinaStudija;
	}
	public void setGodinaStudija(String godinaStudija) {
		this.godinaStudija = godinaStudija;
	}
	public Ispit[] getListaIspita() {
		return listaIspita;
	}
	public void setListaIspita(Ispit[] listaIspita) {
		this.listaIspita = listaIspita;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	
	
	
}
