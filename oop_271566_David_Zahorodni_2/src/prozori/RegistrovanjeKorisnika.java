package prozori;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import alert.AlertKreiranja;
import enumi.Zvanje;
import klase.*;

public class RegistrovanjeKorisnika {
	
	public static Stage window;
	static HBox hboxAddCancel;
	static ComboBox<String> combo;
	static public int broj = 0;
	static String stanjeComboBoxa = "";
	static String stanje;
	static Label obavestenje;
	static Label obavestenjeK;
	static Button add;
	
	public static void prikaz() {
		window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL); 
		window.setTitle("Registrovanje korisnika");
		
		window.setOnCloseRequest(e -> {
			e.consume();
			AlertKreiranja.popUp();
		});
		
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.BASELINE_CENTER);
		
		combo = new ComboBox<>();
		combo.getItems().addAll(
				"Registruj novog admina",
				"Registruj novog nastavnika",
				"Registruj novog studenta"
				);
		combo.setPromptText("Izaberite koga zelite da registrujete");
		vbox.getChildren().add(combo);
		
		
		// opcije za dodati ili odustati
		hboxAddCancel = new HBox();
		hboxAddCancel.setSpacing(2);
		add = new Button("Add");
		
	
		add.setMinWidth(80);
		Button cancel = new Button("Cancel");
		cancel.setOnAction(e -> AlertKreiranja.popUp());
		
		cancel.setMinWidth(80);
		hboxAddCancel.getChildren().addAll(cancel, add);
		
		
		add.getStyleClass().add("plavoDugme");
		cancel.getStyleClass().add("crvenoDugme");
		
		
		// provera da li je korisnik izabrao da promeni tip u combo boxu ili ne i ukoliko jeste onda da se vrednost comboboxa promeni, a ukoliko nije onda da ostane ista kao sto je i bila pre klika na drugu vrednost
		combo.setOnAction(e -> {
			if (broj == 1) {
				comboPopUp();  
			}
			if (broj == 2) {
				broj = 3;
				combo.setValue(stanjeComboBoxa); 
		
				
			}
			if (broj == 3) {
				broj = 1;
				return;
			}
			if (combo.getValue() == "Registruj novog admina") {
				stanjeComboBoxa = combo.getValue();
				prikazAdministratorskogGrida();
				
				broj = 1;
			}
			else if (combo.getValue() == "Registruj novog nastavnika") {
				stanjeComboBoxa = combo.getValue();
				prikazNastavnickogGrida();
				broj = 1;

			}
			else if (combo.getValue() == "Registruj novog studenta") {
				stanjeComboBoxa = combo.getValue();
				prikazStudentskogGrida();
				broj = 1;
			}
			
		});
		
	
		obavestenje = new Label("Ispunite sva polja.");
		obavestenje.setVisible(false);
		obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		
		obavestenjeK = new Label("Korisnicko ime vec postoji, pokusajte sa drugim.");
		obavestenjeK.setVisible(false);
		obavestenjeK.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");

		Scene scena = new Scene(vbox, 440, 300);
		scena.getStylesheets().add("css/registrovanjeKorisnika.css");
		window.setScene(scena);
		window.showAndWait();
	}
	
	private static void prikazAdministratorskogGrida() {
		obavestenje.setVisible(false);
		GridPane gridAdministratora = new GridPane();
		gridAdministratora.setAlignment(Pos.CENTER);
		gridAdministratora.setVgap(6);
		Label label1 = new Label("Unesite ime novog administratora: ");
		TextField ime = new TextField();
		
		Label label2 = new Label("Unesite prezime novog administratora: ");
		TextField prezime = new TextField();
		
		Label label3 = new Label("Unesite korisnicko ime novog administratora: ");
		TextField korisnickoIme = new TextField();
		
		Label label4 = new Label("Unesite lozinku novog administratora: ");
		TextField lozinka = new TextField();
		
		Label obavestenjeZaBrojeve = new Label("Ime i prezime ne mogu da sadrze brojeve.");
		obavestenjeZaBrojeve.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeZaBrojeve.setVisible(false);
		
		gridAdministratora.add(label1, 0, 0);
		gridAdministratora.add(ime, 1, 0);
		gridAdministratora.add(label2, 0, 1);
		gridAdministratora.add(prezime, 1, 1);
		gridAdministratora.add(label3, 0, 2);
		gridAdministratora.add(korisnickoIme, 1, 2);
		gridAdministratora.add(label4, 0, 3);
		gridAdministratora.add(lozinka, 1, 3);
        gridAdministratora.add(obavestenje, 0, 5);
        gridAdministratora.add(obavestenjeK, 0, 6);
        gridAdministratora.add(obavestenjeZaBrojeve, 0, 7);
		
		gridAdministratora.add(hboxAddCancel, 1, 4);
		
		VBox vbox = new VBox(10);
		vbox.getChildren().addAll(combo, gridAdministratora);
		
		add.setOnAction(e -> {
			obavestenje.setVisible(false);
			obavestenjeK.setVisible(false);
			obavestenjeZaBrojeve.setVisible(false);
			FileReader citac = null;
			JSONObject objekat = null;
			boolean pronadjen = false;
			try {
				citac = new FileReader("data/korisnici.json");
				objekat = new JSONObject (new JSONTokener(citac));
				citac.close();
			} catch (IOException | JSONException b) {
				System.out.println("Doslo je do greske prilikom citanja ili rada sa json fajlom.");
			}
			JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
			for (Object recnik: listaAdministratora) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
			}
			// radim jos dva puta slicno da proverim da li je to korisnicko ime dostupno i medju nastavnicima i studentima
			JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
			for (Object recnik: listaNastavnika) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
			}
			JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
			for (Object recnik: listaStudenata) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
			}
			
			if (pronadjen) {
				obavestenjeK.setVisible(true);
			}
			else if (ime.getText().equals("") || prezime.getText().equals("") || korisnickoIme.getText().equals("") || lozinka.getText().equals("")) {
				obavestenje.setVisible(true);
			} else if (ime.getText().matches(".*\\d.*") || prezime.getText().matches(".*\\d.*")) { // proveravam da li ime ili prezime sadrze broj
				obavestenjeZaBrojeve.setVisible(true);
			} else {
				Administrator noviAdministrator = new Administrator(ime.getText(), prezime.getText(), korisnickoIme.getText(), lozinka.getText(), false);
				JSONObject noviAdmin = new JSONObject(noviAdministrator);  
				
				stanje = "administrator";
				upisUJSON(noviAdmin, stanje);
				broj = 0;
			}
		});
		
		Scene scena = new Scene(vbox, 440, 300);
		scena.getStylesheets().add("css/registrovanjeKorisnika.css");
		window.setScene(scena);
	}
	
	private static void prikazNastavnickogGrida() {
		obavestenje.setVisible(false);
		GridPane gridNastavnika = new GridPane();
		gridNastavnika.setAlignment(Pos.CENTER);
		gridNastavnika.setVgap(6);
		Label label1 = new Label("Unesite ime novog nastavnika: ");
		TextField ime = new TextField();
		
		Label label2 = new Label("Unesite prezime novog nastavnika: ");
		TextField prezime = new TextField();
		
		Label label3 = new Label("Unesite korisnicko ime novog nastavnika: ");
		TextField korisnickoIme = new TextField();
		
		Label label4 = new Label("Unesite lozinku novog nastavnika: ");
		TextField lozinka = new TextField();
		
		Label label5 = new Label("Unesite email novog nastavnika: ");
		TextField email = new TextField();
		
		Label label6 = new Label("Izaberite zvanje novog nastavnika: ");
		ChoiceBox<Zvanje> zvanje = new ChoiceBox<>();
		zvanje.getItems().addAll(
				Zvanje.REDOVAN_PROFESOR,
				Zvanje.VANREDNI_PROFESOR,
				Zvanje.ASISTENT,
				Zvanje.DOCENT,
				Zvanje.SARADNIK
				);
		zvanje.setValue(Zvanje.REDOVAN_PROFESOR);

		
		Label obavestenjeZaBrojeve = new Label("Ime i prezime ne mogu da sadrze brojeve.");
		obavestenjeZaBrojeve.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeZaBrojeve.setVisible(false);
		
		gridNastavnika.add(label1, 0, 0);
		gridNastavnika.add(ime, 1, 0);
		gridNastavnika.add(label2, 0, 1);
		gridNastavnika.add(prezime, 1, 1);
		gridNastavnika.add(label3, 0, 2);
		gridNastavnika.add(korisnickoIme, 1, 2);
		gridNastavnika.add(label4, 0, 3);
		gridNastavnika.add(lozinka, 1, 3);
		gridNastavnika.add(label5, 0, 4);
		gridNastavnika.add(email, 1, 4);
		gridNastavnika.add(label6, 0, 5);
		gridNastavnika.add(zvanje, 1, 5);
		gridNastavnika.add(obavestenje, 0, 7);
		gridNastavnika.add(obavestenjeK, 0, 8);
		gridNastavnika.add(obavestenjeZaBrojeve, 0, 9);
		
		gridNastavnika.add(hboxAddCancel, 1, 6);
		
		VBox vbox = new VBox(10);
		vbox.getChildren().addAll(combo, gridNastavnika);
		
		add.setOnAction(e -> {
			obavestenje.setVisible(false);
			obavestenjeK.setVisible(false);
			obavestenjeZaBrojeve.setVisible(false);
			FileReader citac = null;
			JSONObject objekat = null;
			boolean pronadjen = false;
			try {
				citac = new FileReader("data/korisnici.json");
				objekat = new JSONObject (new JSONTokener(citac));
				citac.close();
			} catch (IOException | JSONException b) {
				System.out.println("Doslo je do greske prilikom citanja ili rada sa json fajlom.");
			}
			JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
			for (Object recnik: listaNastavnika) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
			}
			JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
			for (Object recnik: listaAdministratora) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
			}
			JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
			for (Object recnik: listaStudenata) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
			}
			if (pronadjen) {
				obavestenjeK.setVisible(true);
			} else if (ime.getText().equals("") || prezime.getText().equals("") || korisnickoIme.getText().equals("") || lozinka.getText().equals("") || email.getText().equals("")) {
				obavestenje.setVisible(true);
			} else if (ime.getText().matches(".*\\d.*") || prezime.getText().matches(".*\\d.*")) { 
				obavestenjeZaBrojeve.setVisible(true);
			} else {
				Nastavnik noviNastavnik = new Nastavnik(ime.getText(), prezime.getText(), korisnickoIme.getText(), lozinka.getText(), false, email.getText(), zvanje.getValue());
				JSONObject nNastavnik = new JSONObject(noviNastavnik);
				
				stanje = "nastavnik";
				upisUJSON(nNastavnik, stanje);
				broj = 0;
			}
		});
		
		Scene scena = new Scene(vbox, 440, 300);
		scena.getStylesheets().add("css/registrovanjeKorisnika.css");
		window.setScene(scena);
	}
	
	private static void prikazStudentskogGrida() {
		obavestenje.setVisible(false);
		GridPane gridStudenta = new GridPane();
		gridStudenta.setAlignment(Pos.CENTER);
		gridStudenta.setVgap(6);
		Label label1 = new Label("Unesite ime novog studenta: ");
		TextField ime = new TextField();
		
		Label label2 = new Label("Unesite prezime novog studenta: ");
		TextField prezime = new TextField();
		
		Label label3 = new Label("Unesite korisnicko ime novog studenta: ");
		TextField korisnickoIme = new TextField();
		
		Label label4 = new Label("Unesite lozinku novog studenta: ");
		TextField lozinka = new TextField();
		
		Label label5 = new Label("Unesite email novog studenta: ");
		TextField email = new TextField();
		
		Label label6 = new Label("Unesite broj telefona novog studenta: ");
		TextField brojTelefona = new TextField();
		
		
		Label label7 = new Label("Kreiranje indeksa studenta: ");
		
		// za indeks
		Label label8 = new Label("Unesite broj indeksa studenta: "); // stavi da se proveri da li je novi indeks dostupan
		TextField indeksBroj = new TextField();
		
		Label label9 = new Label("Unesite univerzitet studenta: ");
		TextField indeksUniverzitet = new TextField();
		
		Label label10 = new Label("Unesite fakultet studenta: ");
		TextField indeksFakultet = new TextField();
		
		Label label11 = new Label("Unesite smer studenta: ");
		TextField indeksSmer = new TextField();
		
		Label label12 = new Label("Unesite godinu upisa studenta: ");
		TextField indeksGodinaUpisa = new TextField();
		
		Label label13 = new Label("Unesite godinu studija studenta: ");
		TextField indeksGodinaStudija = new TextField();
		
		
		Label obavestenje2 = new Label("Broj telefona, broj indeksa i godina upisa ne mogu sadrzati slova.");
		obavestenje2.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenje2.setVisible(false);
		
		Label obavestenjeI = new Label("Indeks je zauzet, probajte drugi.");
		obavestenjeI.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeI.setVisible(false);
		
		Label obavestenjeZaBrojeve = new Label("Ime i prezime ne mogu da sadrze brojeve.");
		obavestenjeZaBrojeve.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenjeZaBrojeve.setVisible(false);
		
		gridStudenta.add(label1, 0, 0);
		gridStudenta.add(ime, 1, 0);
		gridStudenta.add(label2, 0, 1);
		gridStudenta.add(prezime, 1, 1);
		gridStudenta.add(label3, 0, 2);
		gridStudenta.add(korisnickoIme, 1, 2);
		gridStudenta.add(label4, 0, 3);
		gridStudenta.add(lozinka, 1, 3);
		gridStudenta.add(label5, 0, 4);
		gridStudenta.add(email, 1, 4);
		gridStudenta.add(label6, 0, 5);
		gridStudenta.add(brojTelefona, 1, 5);
		gridStudenta.add(label7, 0, 6);
		gridStudenta.add(label8, 0, 7);
		gridStudenta.add(indeksBroj, 1, 7);
		gridStudenta.add(label9, 0, 8);
		gridStudenta.add(indeksUniverzitet, 1, 8);
		gridStudenta.add(label10, 0, 9);
		gridStudenta.add(indeksFakultet, 1, 9);
		gridStudenta.add(label11, 0, 10);
		gridStudenta.add(indeksSmer, 1, 10);
		gridStudenta.add(label12, 0, 11);
		gridStudenta.add(indeksGodinaUpisa, 1, 11);
		gridStudenta.add(label13, 0, 12);
		gridStudenta.add(indeksGodinaStudija, 1, 12);
		
		gridStudenta.add(obavestenje, 0, 13);
		gridStudenta.add(obavestenje2, 0, 14);
		gridStudenta.add(obavestenjeK, 0, 15);
		gridStudenta.add(obavestenjeI, 0, 16);
		gridStudenta.add(obavestenjeZaBrojeve, 0, 17);
		
		gridStudenta.add(hboxAddCancel, 1, 18);
		
		VBox vbox = new VBox(10);
		vbox.getChildren().addAll(combo, gridStudenta);
		
		add.setOnAction(e -> {
			obavestenje.setVisible(false);
			obavestenje2.setVisible(false);
			obavestenjeK.setVisible(false);
			obavestenjeI.setVisible(false);
			obavestenjeZaBrojeve.setVisible(false);
			FileReader citac = null;
			JSONObject objekat = null;
			boolean pronadjen = false;
			boolean indeksNadjen = false;
			try {
				citac = new FileReader("data/korisnici.json");
				objekat = new JSONObject (new JSONTokener(citac));
				citac.close();
			} catch (IOException | JSONException b) {
				System.out.println("Doslo je do greske prilikom citanja ili rada sa json fajlom.");
			}
			JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
			for (Object recnik: listaStudenata) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
				if (String.valueOf((((JSONObject)recnik2.get("indeks")).getInt("broj"))).equals(indeksBroj.getText())) {
					indeksNadjen = true;
					break;
				}
			}
			JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
			for (Object recnik: listaAdministratora) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
			}
			JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
			for (Object recnik: listaNastavnika) {
				JSONObject recnik2 = (JSONObject) recnik;
				if (recnik2.getString("korisnickoIme").equals(korisnickoIme.getText())) {
					pronadjen = true;
					break;
				}
			}
			
			if (pronadjen) {
				obavestenjeK.setVisible(true);
			} else if (indeksNadjen) {
				obavestenjeI.setVisible(true);
			} else if (ime.getText().equals("") || prezime.getText().equals("") || korisnickoIme.getText().equals("") || lozinka.getText().equals("") || email.getText().equals("") || brojTelefona.getText().equals("")) {
				obavestenje.setVisible(true);
			} else if (ime.getText().matches(".*\\d.*") || prezime.getText().matches(".*\\d.*")) { // proveravam da li ime ili prezime sadrze broj
				obavestenjeZaBrojeve.setVisible(true);
			} else if (indeksBroj.getText().equals("") || indeksUniverzitet.getText().equals("") || indeksFakultet.getText().equals("") || indeksSmer.getText().equals("") || indeksGodinaUpisa.getText().equals("") || indeksGodinaStudija.getText().equals("")) {
				obavestenje.setVisible(true);
			} else if (!brojTelefona.getText().matches("[0-9]+")) { //PODSETNIK MENI: pazi, mora uzvicnik ispred jer ti matches() vraca false ukoliko ne nadje pa ga mi sa ! okrenemo na true da bi usao u blok koda
				obavestenje2.setVisible(true);
			} else if (!indeksBroj.getText().matches("[0-9]+") || !indeksGodinaUpisa.getText().matches("[0-9]+")) {
				obavestenje2.setVisible(true);
			}
			
			else {
				JSONObject indeks = new JSONObject();
            	indeks.put("broj", Integer.parseInt(indeksBroj.getText()));
            	indeks.put("univerzitet", indeksUniverzitet.getText());
            	indeks.put("fakultet", indeksFakultet.getText());
            	indeks.put("smer", indeksSmer.getText());
            	indeks.put("godina upisa", Integer.parseInt(indeksGodinaUpisa.getText()));
            	indeks.put("godina studija", indeksGodinaStudija.getText());
            	//formiram ArrayListu pod kljucem "ispiti" 
            	JSONArray listaIspita = new JSONArray();

        		indeks.put("ispiti", listaIspita);

            	
            	// formiram string za kljuc "student" od vrednosti polja ime i prezime
            	String studentPodatak = ime.getText() + " " + prezime.getText();
            	indeks.put("student", studentPodatak);
            	
				Student noviStudent = new Student(ime.getText(), prezime.getText(), korisnickoIme.getText(), lozinka.getText(), false, email.getText(), Long.parseLong(brojTelefona.getText()), indeks);
				JSONObject nStudent = new JSONObject(noviStudent);
				
				stanje = "student";
				upisUJSON(nStudent, stanje);
				broj = 0;
			}
		});
		
		Scene scena = new Scene(vbox, 850, 700);
		scena.getStylesheets().add("css/registrovanjeKorisnika.css");
		window.setScene(scena);
	}
	private static void comboPopUp() {
		Stage window1 = new Stage();
		window1.initModality(Modality.APPLICATION_MODAL);
		window1.setTitle("Upozorenje!");
		
		VBox vbox = new VBox(10);
		vbox.setAlignment(Pos.CENTER);
		
		Label labela = new Label("Da li ste sigurni da zelite da promenite tip?");
		
		HBox hbox = new HBox();
		hbox.setSpacing(7);
		Button btn1 = new Button("Da");
		btn1.setMinWidth(50);
		btn1.setOnAction(e -> {
			broj = 0;
			window1.close();
		});
		Button btn2 = new Button("Ne");
		btn2.setMinWidth(50);
		btn2.setOnAction(e -> {
			broj = 2;
			window1.close();
		});
		hbox.getChildren().addAll(btn1, btn2);
		hbox.setAlignment(Pos.CENTER);
		
		btn1.getStyleClass().add("plavoDugme");
		btn2.getStyleClass().add("crvenoDugme");
		
		vbox.getChildren().addAll(labela, hbox);
		
		Scene scena = new Scene(vbox, 280, 150);
		scena.getStylesheets().add("css/registrovanjeKorisnika.css");
		window1.setScene(scena);
		window1.showAndWait();
	}
	// funkcija za upis napravljenog korisnika u JSON
	private static void upisUJSON(JSONObject objekat, String stanje) {
		FileReader in = null;
		PrintWriter out = null;
		
		try {
			in = new FileReader("data/korisnici.json");
			JSONObject objekatJSON = new JSONObject(new JSONTokener(in));
			in.close();
			if (stanje == "administrator") {
				objekatJSON.accumulate("administratori", objekat);
			} else if (stanje == "nastavnik") {
				objekatJSON.accumulate("nastavnici", objekat);
			} else if (stanje == "student") {
				objekatJSON.accumulate("studenti", objekat);
			}
			out = new PrintWriter("data/korisnici.json");
			out.write(objekatJSON.toString(4));
			out.close();
		} catch(IOException | JSONException e) {
			System.out.println("Doslo je do greske prilikom citanje ili upisa json fajla.");
		}
		window.close();
	}
	
	
}
