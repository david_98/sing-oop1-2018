package prozori;

import java.io.FileReader;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import klase.Student;

public class PretragaStudenata {
		
	public static void prikaz() {
		Stage window = new Stage();
		window.setTitle("Pretraga studenta");
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(20, 0, 0, 0));
		Label obavestenje = new Label("Nema poklapanja, pokusajte ponovo.");
		obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenje.setVisible(false);

		
		HBox hbox = new HBox();
		Label labela = new Label("Unesite informacije za pretragu: ");
		labela.setStyle("-fx-text-fill: #E8E8E8; -fx-font-size: 16px");
		TextField info = new TextField();
		info.setPromptText("  ime, prezime...");
		Button pretrazi = new Button("Pretrazi");
		
		hbox.setSpacing(5);
		hbox.getChildren().addAll(labela, info, pretrazi);
	
		vbox.getChildren().addAll(hbox, obavestenje);
		VBox vbox2 = new VBox();

		pretrazi.setOnAction(e -> {
			obavestenje.setVisible(false);

			if (info.getText().equals("")) {
				obavestenje.setVisible(true);
			} else {
				TableView<Student> tabela = new TableView<>();
				
				if (vbox.getChildren().contains(vbox2)) {
					vbox2.getChildren().clear();
					vbox.getChildren().remove(2);
				}
				
				boolean pronadjen = false;
				FileReader citac = null;
				FileReader citac2 = null;
				JSONObject objekat = null; // za korisnike
				JSONArray predmeti = null; // predmete
				ObservableList<Student> lista = FXCollections.observableArrayList();
				try {
					citac = new FileReader("data/korisnici.json");
					citac2 = new FileReader("data/predmeti.json");
					objekat = new JSONObject(new JSONTokener(citac));
					predmeti = new JSONArray(new JSONTokener(citac2));
					citac.close();
					citac2.close();
				} catch (IOException | JSONException b) {
					System.out.println("Greska prilikom citanja iz json fajla.");
				}
				JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
				for (Object recnik: listaStudenata) {
					JSONObject recnik2 = (JSONObject) recnik;
					if (info.getText().toLowerCase().equals(recnik2.getString("ime").toLowerCase()) || info.getText().toLowerCase().equals(recnik2.getString("prezime").toLowerCase()) || info.getText().equals(String.valueOf(((JSONObject)recnik2.get("indeks")).getInt("broj")))) {
//						
						lista.add(kreiranjeStudentaZaListu(recnik2));
						pronadjen = true;
					} 
				}
				// vrsim proveru u slucaju da je nastavnik ukucao predmet
				if (!pronadjen) {
					for (Object predmet: predmeti) {
						JSONObject recnik = (JSONObject) predmet;
						if (recnik.getString("naziv").equals(info.getText())) {
							for (Object recnik2: listaStudenata) {
								JSONObject recnik3 = (JSONObject) recnik2;
								if (((JSONObject)recnik3.get("indeks")).get("smer").equals(recnik.get("smer")) && ((JSONObject)recnik3.get("indeks")).get("godina studija").equals(recnik.get("godinaStudija"))) {
									lista.add(kreiranjeStudentaZaListu(recnik3));
									pronadjen = true;
								}
							}
						}
					}
				}
				
				
				if (!pronadjen) {
					obavestenje.setVisible(true);
				} else {
					tabela.setItems(lista);
					
		            TableColumn<Student, String> kolona1 = new TableColumn<>("Ime");
		            kolona1.setMinWidth(100);
		            kolona1.setCellValueFactory(new PropertyValueFactory<>("ime"));
		            
		            TableColumn<Student, String> kolona2 = new TableColumn<>("Prezime");
		            kolona2.setMinWidth(100);
		            kolona2.setCellValueFactory(new PropertyValueFactory<>("prezime"));
		            
		            TableColumn<Student, JSONObject> kolona3 = new TableColumn<>("Indeks -->");
		            kolona3.setMinWidth(100);
		            kolona3.setCellValueFactory(new PropertyValueFactory<>("indeks"));
		            
		            TableColumn<Student, String> kolona4 = new TableColumn<>("Lozinka");
		            kolona4.setMinWidth(100);
		            kolona4.setCellValueFactory(new PropertyValueFactory<>("lozinka"));
		            
		            TableColumn<Student, Boolean> kolona5 = new TableColumn<>("Obrisan");
		            kolona5.setMinWidth(100);
		            kolona5.setCellValueFactory(new PropertyValueFactory<>("obrisan"));
		            
		            TableColumn<Student, Long> kolona6 = new TableColumn<>("Broj telefona");
		            kolona6.setMinWidth(100);
		            kolona6.setCellValueFactory(new PropertyValueFactory<>("brojTelefona"));
		            
		            TableColumn<Student, String> kolona7 = new TableColumn<>("Korisnicko ime");
		            kolona7.setMinWidth(100);
		            kolona7.setCellValueFactory(new PropertyValueFactory<>("korisnickoIme"));
		            
		            TableColumn<Student, String> kolona8 = new TableColumn<>("Email");
		            kolona8.setMinWidth(150);
		            kolona8.setCellValueFactory(new PropertyValueFactory<>("email"));
					
		            tabela.getColumns().addAll(kolona1, kolona2, kolona3, kolona4, kolona5, kolona6, kolona7, kolona8);
		            vbox2.getChildren().add(tabela);
		            vbox.getChildren().add(vbox2);
				}
			}
		});
		
		Scene scena = new Scene(vbox, 850, 250);
		scena.getStylesheets().add("css/pretragaStudenata.css");
		window.setScene(scena);
		window.show();
	} 
	private static Student kreiranjeStudentaZaListu(JSONObject recnik) {
//		Student studentRecnik = (Student) recnik2; //PODSETNIK MENI: ovo se ne moze uraditi jer serijalizacija iz jsonobjekta u objekat neke klase nije moguca (moguce da je tako mozda delom jer jsonobjekat kad se formira na osnovu nje ne uzima metode)
		Student student = new Student();
		student.setIme(recnik.getString("ime"));
		student.setPrezime(recnik.getString("prezime"));
		// kreiramo i popunjavamo JSONObject indeks za vrednost polja indeks objekta Student
		JSONObject indeks = new JSONObject();
		indeks.put("fakultet", ((JSONObject)recnik.get("indeks")).get("fakultet"));
		indeks.put("smer", ((JSONObject)recnik.get("indeks")).get("smer"));
        indeks.put("broj", ((JSONObject)recnik.get("indeks")).get("broj"));
        indeks.put("student", ((JSONObject)recnik.get("indeks")).get("student"));
        indeks.put("univerzitet", ((JSONObject)recnik.get("indeks")).get("univerzitet"));
        indeks.put("ispiti", ((JSONObject)recnik.get("indeks")).get("ispiti"));
        indeks.put("godina upisa", ((JSONObject)recnik.get("indeks")).get("godina upisa"));
        indeks.put("godina studija", ((JSONObject)recnik.get("indeks")).get("godina studija"));
        
		student.setIndeks(indeks);
		student.setLozinka(recnik.getString("lozinka"));
		student.setObrisan(recnik.getBoolean("obrisan"));
		student.setBrojTelefona(recnik.getLong("brojTelefona"));
		student.setKorisnickoIme(recnik.getString("korisnickoIme"));
		student.setEmail(recnik.getString("email"));
		
		return student;
	}
}
