package prozori;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import enumi.IspitniRok;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import klase.Ispit;

public class UpisOcena {
	static Stage window1;
	public static void prikaz(JSONObject korisnici) {
		window1 = new Stage();
		window1.setTitle("Upis ocene");
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
	    GridPane gpane = new GridPane();
	    gpane.setVgap(10);
	    gpane.setHgap(10);
	    Label labelaRok = new Label("Pretrazi spram ispitnog roka:");
	    labelaRok.getStyleClass().add("labela");
	    ComboBox<IspitniRok> ispitniRok = new ComboBox<>();
	    ispitniRok.getItems().addAll(
	    		IspitniRok.JANUARSKI,
	    		IspitniRok.FEBRUARSKI,
	    		IspitniRok.MARTOVSKI,
	    		IspitniRok.APRILSKI,
	    		IspitniRok.MAJSKI,
	    		IspitniRok.JUNSKI,
	    		IspitniRok.JULSKI,
	    		IspitniRok.SEPTEMBARSKI,
	    		IspitniRok.OKTOBARSKI,
	    		IspitniRok.NOVEMBARSKI,
	    		IspitniRok.DECEMBARSKI
	    		);
	    
	   Label labelaPredmet = new Label("Pretrazi spram naziva predmeta:");
	   labelaPredmet.getStyleClass().add("labela");
	   ComboBox<String> predmet = new ComboBox<>();
	   // kreiram listu sa predmetima
	   FileReader citac = null;
	   JSONArray lista = null;
	   try {
		   citac = new FileReader("data/predmeti.json");
		   lista = new JSONArray(new JSONTokener(citac));
		   citac.close();
	   } catch (IOException | JSONException b) {
		   System.out.println("Greska prilikom citanja iz json fajla.");
	   }
	   ArrayList<String> listaSaPredmetima = new ArrayList<>();
	   for (Object predmet1: lista) {
		   JSONObject predmet2 = (JSONObject) predmet1;
		   listaSaPredmetima.add(String.valueOf(predmet2.get("naziv")));
	   }
	   predmet.getItems().addAll(listaSaPredmetima);
	   
	   Label detaljnaPretraga = new Label("Pretraga po indeksu, imenu ili prezimenu:");
	   detaljnaPretraga.getStyleClass().add("labela");
	   TextField tekst = new TextField();
	   
	   TableView<Ispit> tabela = new TableView<>();
	   TableColumn<Ispit, Integer> kolonaIndeks = new TableColumn<>("Indeks");
	   kolonaIndeks.setCellValueFactory(new PropertyValueFactory<>("indeks"));
	   
	   TableColumn<Ispit, String> kolonaPredmet = new TableColumn<>("Predmet");
	   kolonaPredmet.setCellValueFactory(new PropertyValueFactory<>("predmet"));
	   
	   TableColumn<Ispit, IspitniRok> kolonaRok = new TableColumn<>("Rok");
	   kolonaRok.setCellValueFactory(new PropertyValueFactory<>("rok"));
	   
	   tabela.getColumns().addAll(kolonaIndeks, kolonaPredmet, kolonaRok);
	   tabela.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> upisOcene(newValue, korisnici));
	   
	   Button btn = new Button("Pretrazi");
	   btn.getStyleClass().add("dugme");
	   Label obavestenje = new Label("Nije pronadjena ni jedna prijava sa tim podacima.");
	   obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
	   obavestenje.setVisible(false);
	   btn.setOnAction(e -> {
		   tabela.setItems(null); // kako bi se tabela prilikom svake pretrage prvo ocistila
		   ArrayList<JSONObject> listaPogodaka = new ArrayList<>();
		   obavestenje.setVisible(false);
		   JSONArray listaStudenata = (JSONArray) korisnici.get("studenti");
		   for (Object student: listaStudenata) {
			   JSONObject student1 = (JSONObject) student;
			   if (student1.getBoolean("obrisan")) {
				   continue;
			   }
			   JSONObject indeks = (JSONObject) student1.get("indeks");
			   JSONArray listaIspita = (JSONArray) indeks.get("ispiti");
			   for (Object ispit: listaIspita) {
				   boolean poklapanje = true;
			       JSONObject ispit2 = (JSONObject) ispit;
			       boolean vecDodato = false; 
			       if (ispit2.getBoolean("ponisten")) {
			    	   poklapanje = false;
			       } else if (Integer.parseInt(String.valueOf(ispit2.get("ocena"))) != 0) {
			    	   poklapanje = false;
			       } else if (ispitniRok.getValue() != null || predmet.getValue() != null || !tekst.getText().equals("")) {
			    	   if (ispitniRok.getValue() != null) {
			    		   if (IspitniRok.valueOf(String.valueOf(ispit2.get("rok"))).equals(ispitniRok.getValue())) {
				    		   poklapanje = true;
				    	   } else {
				    		   poklapanje = false;
				    	   }
			    	   }
			    	   if (poklapanje) {
			    		   if (predmet.getValue() != null) {
					    	   if (String.valueOf(ispit2.get("predmet")).equals(predmet.getValue())) {
					    		   poklapanje = true;
					    	   } else {
					    		   poklapanje = false;
					    	   }
			    		   }
			    	   }
			    	   if (poklapanje) {
			    		   if (!tekst.getText().equals("")) {
			    			   String studentInfo = String.valueOf(indeks.get("broj")) + String.valueOf(student1.get("ime")).toLowerCase() + String.valueOf(student1.get("prezime")).toLowerCase();
				        	   String unetiInfo = tekst.getText().toLowerCase();
				        	   String[] listaInfo = unetiInfo.split(" ");
				        	   for (String element: listaInfo) {
				        		   if (!studentInfo.contains(element)) {
				        			   poklapanje = false;
				        			   break;
				        		   } 
				        	   } 
				           }
		    		   }
			    	   if (poklapanje) {
			    		   listaPogodaka.add(ispit2);
			    		   vecDodato = true;
			    	   }
		    	   }
	        	 if (poklapanje && !vecDodato) {
	        		 listaPogodaka.add(ispit2);
	        	 }
		           
			   }
			   
		   }
		   if (listaPogodaka.size() == 0) {
			   obavestenje.setVisible(true);
		   } else {
			   // kreiram objekte tipa Ispit i dodajem ih u observableList-u za TableView
			   ObservableList<Ispit> listaZaTabelu = FXCollections.observableArrayList();
			   for (JSONObject ispit: listaPogodaka) {
				   Ispit ispit2 = new Ispit(ispit.getInt("indeks"), ispit.getInt("indeks"), ispit.getInt("ocena"), LocalDate.parse(ispit.getString("datumPolaganja")), ispit.getBoolean("ponisten"), ispit.getString("predmet"), IspitniRok.valueOf(ispit.getString("rok")));
				   listaZaTabelu.add(ispit2);
			   }
			   tabela.setItems(listaZaTabelu);
		   }
	   });
	   
	   gpane.add(labelaRok, 0, 0);
	   gpane.add(ispitniRok, 1, 0);
	   gpane.add(labelaPredmet, 0, 1);
	   gpane.add(predmet, 1, 1);
	   gpane.add(detaljnaPretraga, 0, 2);
	   gpane.add(tekst, 1, 2);
	   gpane.add(obavestenje, 0, 3);
	   gpane.add(btn, 1, 3);
	   
	   
	   vbox.getChildren().addAll(gpane, tabela);
	   Scene scena = new Scene(vbox, 450, 325);
	   scena.getStylesheets().add("css/upisOcena.css");
	   window1.setScene(scena);
	   window1.show();
	}
	private static void upisOcene(Ispit ispit, JSONObject korisnici) {
		Stage window = new Stage();
		window.setTitle("Unos bodova");
		window.initModality(Modality.APPLICATION_MODAL);
		VBox vbox = new VBox(10);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		HBox hbox = new HBox();
		hbox.setSpacing(6);
		TextField bodovi = new TextField();
		bodovi.setPromptText("bodovi");
		bodovi.setFocusTraversable(false);
		
		Label obavestenje = new Label("Bodovi mogu biti iskljucivo brojevi, od 0-100."); // bodovi su celobrojni brojevi
		obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenje.setVisible(false);
		
		Button btn = new Button("Postavi ocenu.");
		btn.getStyleClass().add("dugme");
		btn.setMinWidth(120);
		btn.setOnAction(e -> {
			if (!bodovi.getText().matches("[0-9]+") || Integer.parseInt(bodovi.getText()) < 0 || Integer.parseInt(bodovi.getText()) > 100) {
				obavestenje.setVisible(true);
			} else {
				JSONArray listaStudenata = (JSONArray) korisnici.get("studenti");
				for (Object student: listaStudenata) {
					JSONObject student1 = (JSONObject) student;
					if (student1.getBoolean("obrisan")) {
						continue;
					}
					JSONObject indeks = (JSONObject) student1.get("indeks");
					JSONArray listaIspita = (JSONArray) indeks.get("ispiti");
					for (Object ispit1: listaIspita) {
						JSONObject ispit2 = (JSONObject) ispit1;
						if (ispit2.getBoolean("ponisten")) {
							continue;
						} else if (String.valueOf(ispit2.getInt("indeks")).equals(String.valueOf(ispit.getIndeks())) && ispit2.getString("predmet").equals(ispit.getPredmet()) && IspitniRok.valueOf(String.valueOf(ispit2.get("rok"))).equals(ispit.getRok())) {
					    	
					    	int konOcena = 0;
					    	if (Integer.parseInt(bodovi.getText()) >= 0 && Integer.parseInt(bodovi.getText()) <= 50 ) {
					    		konOcena = 5;
					    	} else if (Integer.parseInt(bodovi.getText()) >= 51 && Integer.parseInt(bodovi.getText()) <= 60 ) {
					    		konOcena = 6;
					    	} else if (Integer.parseInt(bodovi.getText()) >= 61 && Integer.parseInt(bodovi.getText()) <= 70 ) {
					    		konOcena = 7;
					    	} else if (Integer.parseInt(bodovi.getText()) >= 71 && Integer.parseInt(bodovi.getText()) <= 80 ) {
					    		konOcena = 8;
					    	} else if (Integer.parseInt(bodovi.getText()) >= 81 && Integer.parseInt(bodovi.getText()) <= 90 ) {
					    		konOcena = 9;
					    	} else if (Integer.parseInt(bodovi.getText()) >= 91 && Integer.parseInt(bodovi.getText()) <= 100 ) {
					    	    konOcena = 10;
					    	}
					    	
					    	ispit2.put("bodovi", Integer.parseInt(bodovi.getText()));
					    	ispit2.put("ocena", konOcena);
					    	ispit2.put("datumPolaganja", LocalDate.now());
					    	
					    	PrintWriter upis = null;
					    	try {
					    		upis = new PrintWriter("data/korisnici.json");
					    		upis.write(korisnici.toString(4));
					    		upis.close();
					    	} catch(IOException | JSONException b) {
					    		System.out.println("Greska prilikom upisa u json fajl.");
					    	}
					    	window.close();
					        window1.close();
					    	prikaz(korisnici);
					    	return;
					    }
					}
					
				}
			}
		});
		hbox.getChildren().addAll(bodovi, btn);
		
		vbox.getChildren().addAll(hbox, obavestenje);
		Scene scena = new Scene(vbox, 250, 100);
		scena.getStylesheets().add("css/upisOcena.css");
		window.setScene(scena);
		window.showAndWait();
	}
}
