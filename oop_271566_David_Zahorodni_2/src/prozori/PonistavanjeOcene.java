package prozori;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import enumi.IspitniRok;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import klase.Ispit;

public class PonistavanjeOcene {
	
	static Stage window;
	
	public static void prikaz(JSONObject student, JSONObject objekat) {
		window = new Stage();
		window.setTitle("Ponistavanje ocene");
	    VBox vbox = new VBox(20);
	    Label labela = new Label("Izaberite ispit koji zelite da ponistite:");
	    labela.setStyle("-fx-text-fill: #E8E8E8; -fx-font-weight: bold;");
	    labela.setPadding(new Insets(10, 0, 0, 10));
	    Label labela2 = new Label("Nema predmeta koji bi mogao da se ponisti.");
	    labela2.setStyle("-fx-text-fill: #E8E8E8; -fx-font-weight: bold;");
	    labela2.setPadding(new Insets(0, 0, 0, 10));
	    TableView<Ispit> tabela = new TableView<>();
	    tabela.setVisible(false);
	    
	    // izvlacenje ispita koje ovaj student moze da ponisti
	    JSONArray sviIspiti = ((JSONObject)student.get("indeks")).getJSONArray("ispiti");
	    ObservableList<Ispit> listaIspita = FXCollections.observableArrayList();
	    for (Object ispit: sviIspiti) {
	    	JSONObject ispit2 = (JSONObject) ispit;
	    	if (ispit2.getInt("bodovi") != 0 && ispit2.getInt("ocena") != 0 && ispit2.getBoolean("ponisten") == false) {
	    		 labela2.setVisible(false);
	    		 tabela.setVisible(true);
	    		 Ispit nIspit = new Ispit(ispit2.getInt("indeks"), ispit2.getInt("bodovi"), ispit2.getInt("ocena"), LocalDate.parse((CharSequence) ispit2.get("datumPolaganja")), ispit2.getBoolean("ponisten"), ispit2.getString("predmet"), IspitniRok.valueOf(String.valueOf(ispit2.get("rok"))));
	    		 listaIspita.add(nIspit);
	    	}
	    }
	    tabela.setItems(listaIspita);

	    //kreiram kolone
	    TableColumn<Ispit, String> imePredmeta = new TableColumn<>("Ime predmeta");
	    imePredmeta.setCellValueFactory(new PropertyValueFactory<>("predmet"));
	    
	    TableColumn<Ispit, Integer> ocenaPredmeta = new TableColumn<>("Ocena predmeta");
	    ocenaPredmeta.setCellValueFactory(new PropertyValueFactory<>("ocena"));
	    
	    tabela.getColumns().addAll(imePredmeta, ocenaPredmeta);
	    
	    tabela.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
	    	popUp(newValue, sviIspiti, objekat, student);
	    });
	    
	    vbox.getChildren().addAll(labela, labela2, tabela);
	    Scene scena = new Scene(vbox, 400, 200);
	    scena.getStylesheets().add("css/ponistavanjeOcene.css");
	    window.setScene(scena);
	    window.show();
	}
	// trazim potvrdu za ponistavanje ocene
	private static void popUp(Ispit ispit, JSONArray sviIspiti, JSONObject objekat, JSONObject student) {
		Stage window = new Stage();
		window.setTitle("Potvrda");
		window.initModality(Modality.APPLICATION_MODAL);
		VBox vbox = new VBox(10);
		vbox.setAlignment(Pos.CENTER);
		Label labela = new Label("Ponisti ocenu.");
		labela.setStyle("-fx-text-fill: #E8E8E8; -fx-font-weight: bold;");
		HBox hbox = new HBox();
		hbox.setSpacing(10);
		hbox.setAlignment(Pos.CENTER);
		Button btn1 = new Button("Da");
		btn1.setMinWidth(50);
		btn1.setId("plavoDugme");
		btn1.setOnAction(e -> {
			for (Object ispitPojedinacno: sviIspiti) {
				JSONObject ispit2 = (JSONObject) ispitPojedinacno;
				if (ispit2.getString("predmet").equals(ispit.getPredmet()) && IspitniRok.valueOf(String.valueOf(ispit2.get("rok"))).equals(ispit.getRok())) {
					ispit2.put("ponisten", true);
					break;
				}
			}
			PrintWriter upis = null;
			try {
				upis = new PrintWriter("data/korisnici.json");
				upis.write(objekat.toString(4));
				upis.close();
			} catch(IOException | JSONException b) {
				System.out.println("Greska prilikom upisa u json fajl.");
			}
			window.close();
			PonistavanjeOcene.window.close();
			prikaz(student, objekat);
		});
		Button btn2 = new Button("Ne");
		btn2.setMinWidth(50);
		btn2.setId("crvenoDugme");
		btn2.setOnAction(e -> {
			window.close();
			PonistavanjeOcene.window.close();
			prikaz(student, objekat);
		});
		hbox.getChildren().addAll(btn1, btn2);
		vbox.getChildren().addAll(labela, hbox);
		Scene scena = new Scene(vbox, 220, 150);
		scena.getStylesheets().add("css/ponistavanjeOcene.css");
		window.setScene(scena);
		window.showAndWait();
	}
}
