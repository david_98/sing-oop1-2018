package prozori;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.JSONArray;
import org.json.JSONException;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class BrisanjeKorisnika {
	
	static Stage window;
	
	public static void prikaz() {
		window = new Stage();
		window.setTitle("Brisanje korisnika");
		
		VBox vbox = new VBox(15);
		vbox.setAlignment(Pos.CENTER);
		
		Label labela = new Label("Unesite korisnicko ime korisnika kojeg bi zeleli da obrisete.");
		
		Label poruka = new Label("Uneti korisnik nije pronadjen, pokusajte ponovo.");
		poruka.setId("poruka");
		poruka.setVisible(false);
		
		
		HBox hbox = new HBox();
		hbox.setSpacing(10);
		TextField korisnickoIme = new TextField();
		Button btn1 = new Button("Pretrazi");
		btn1.getStyleClass().add("plavoDugme");
		
		btn1.setOnAction(e -> {
			boolean pronadjen = false;
			FileReader citac = null;
			
			try {
				citac = new FileReader("data/korisnici.json");
				JSONObject objekat = new JSONObject(new JSONTokener(citac));
				citac.close();
				// vrsim proveru za listu pod kljucem administratori
				JSONArray listaAdministratora = (JSONArray) objekat.get("administratori");
				for (Object recnik: listaAdministratora) {
					JSONObject recnik2 = (JSONObject) recnik;
					if (korisnickoIme.getText().equals(recnik2.getString("korisnickoIme")) && !korisnickoIme.getText().equals(PrijavljivanjeNaSistem.trenutniKorisnik.getString("korisnickoIme"))) {
						JSONObject izabraniKorisnik = recnik2;
						pronadjen = true;
						nadjen(izabraniKorisnik, objekat);
					}
				}
				// vrsim proveru za listu pod kljuce nastavnici
				JSONArray listaNastavnika = (JSONArray) objekat.get("nastavnici");
				for (Object recnik: listaNastavnika) {
					JSONObject recnik2 = (JSONObject) recnik;
					if (korisnickoIme.getText().equals(recnik2.getString("korisnickoIme"))) {
						JSONObject izabraniKorisnik = recnik2;
						pronadjen = true;
						nadjen(izabraniKorisnik, objekat);
					}
				}
				// vrsim proveru za listu pod kljucem studenti
				JSONArray listaStudenata = (JSONArray) objekat.get("studenti");
				for (Object recnik: listaStudenata) {
					JSONObject recnik2 = (JSONObject) recnik;
					if (korisnickoIme.getText().equals(recnik2.getString("korisnickoIme"))) {
						JSONObject izabraniKorisnik = recnik2;
						pronadjen = true;
						nadjen(izabraniKorisnik, objekat);
					}
				}
				if (!pronadjen) {
					poruka.setVisible(true);
					korisnickoIme.clear();
					korisnickoIme.requestFocus();
				}
			} catch (IOException | JSONException b) {
				System.out.println("Doslo je do greske prilikom citanja ili rada sa JSON fajlom.");
			}
		});
		hbox.getChildren().addAll(korisnickoIme, btn1);
		korisnickoIme.setMinWidth(80);
		btn1.setMinWidth(80);
		hbox.setAlignment(Pos.CENTER);
		
		
		vbox.getChildren().addAll(labela, hbox, poruka);
		Scene scena = new Scene(vbox, 450, 150);
		scena.getStylesheets().add("css/brisanjeKorisnika.css");
		window.setScene(scena);
		window.show();
	}
	private static void nadjen(JSONObject izabraniKorisnik, JSONObject objekat) {
		HBox hbox = new HBox();
		hbox.setSpacing(10);
		Label labela = new Label("Korisnik je pronadjen.");
		Button btn = new Button("Obrisi");
		btn.getStyleClass().add("plavoDugme");
		btn.setMinWidth(80);
		btn.setOnAction(e -> upozorenje(izabraniKorisnik, objekat));
		hbox.getChildren().addAll(labela, btn);
		hbox.setAlignment(Pos.CENTER);
		
		Scene scena = new Scene(hbox, 450, 150);
		scena.getStylesheets().add("css/brisanjeKorisnika.css");
		window.setScene(scena);
		window.show(); // ZA MENE: buduci da je window statican i globalan, dovoljno je postaviti window.show samo u prvoj funkciji
	}
	private static void upozorenje(JSONObject izabraniKorisnik, JSONObject objekat) {
		VBox vbox = new VBox(10);
		vbox.setAlignment(Pos.CENTER);
		Label labela = new Label("Da li ste sigurni da zelite da obrisete ovog korisnika?");
		HBox hbox = new HBox();
		hbox.setSpacing(10);
		hbox.setAlignment(Pos.CENTER);
		Button btn1 = new Button("Da");
		btn1.getStyleClass().add("crvenoDugme");
		btn1.setMinWidth(40);
		btn1.setOnAction(e -> {
			izabraniKorisnik.put("obrisan", true);
			PrintWriter upis = null;
			try {
				upis = new PrintWriter("data/korisnici.json");
				upis.write(objekat.toString(4));
				upis.close();
			} catch (IOException | JSONException b) {
				System.out.println("Greska prilikom upisa izmenjenog json objekta u korisnici.json.");
			}
			window.close();
		});
		Button btn2 = new Button("Ne");
		btn2.getStyleClass().add("plavoDugme");
		btn2.setOnAction(e -> window.close());
		btn2.setMinWidth(40);
		
		hbox.getChildren().addAll(btn2, btn1);
		vbox.getChildren().addAll(labela, hbox);
		
		Scene scena = new Scene(vbox, 315, 100);
		scena.getStylesheets().add("css/brisanjeKorisnika.css");
		window.setScene(scena);
	}
}
