package prozori;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import klase.Ispit;
import enumi.IspitniRok;

public class PrijavaIspita {
	
	static String izabraniPredmet;
	// prikaz za prijavu ispita
	public static void prikaz(JSONObject student, JSONObject objekat) {
		Stage window = new Stage();
		window.setTitle("Prija ispita");
		
		VBox vbox = new VBox(20);
		
		Label labela = new Label("Izaberite predmet koji zelite da prijavite:");
		labela.setPadding(new Insets(10, 0, 0, 10));
		
		HBox hbox = new HBox();
		hbox.setSpacing(10);
		hbox.setPadding(new Insets(0, 0, 0, 10));
		Label labela2 = new Label("Izaberite ispitni rok: ");
		ComboBox<IspitniRok> cbox = new ComboBox<>();
		cbox.setPromptText("Rokovi...");
		cbox.setVisible(false);
		hbox.getChildren().addAll(labela2, cbox);
		
		ArrayList<IspitniRok> listaRokova= new ArrayList<>();
		
		ListView<String> lista = new ListView<>();
		lista.setPadding(new Insets(10, 10, 10, 10));
		lista.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
			izabraniPredmet = newValue;
			
			cbox.setVisible(true);
			cbox.getItems().clear();
			listaRokova.clear();
		    listaRokova.add(IspitniRok.JANUARSKI);
			listaRokova.add(IspitniRok.FEBRUARSKI);
			listaRokova.add(IspitniRok.MARTOVSKI);
			listaRokova.add(IspitniRok.APRILSKI);
			listaRokova.add(IspitniRok.MAJSKI);
			listaRokova.add(IspitniRok.JUNSKI);
			listaRokova.add(IspitniRok.JULSKI);
			listaRokova.add(IspitniRok.SEPTEMBARSKI);
			listaRokova.add(IspitniRok.OKTOBARSKI);
			listaRokova.add(IspitniRok.NOVEMBARSKI);
			listaRokova.add(IspitniRok.DECEMBARSKI);
			
			// vrsim proveru
			if (((JSONObject)student.get("indeks")).getJSONArray("ispiti").length() == 0) {
				cbox.getItems().addAll(IspitniRok.JANUARSKI, IspitniRok.FEBRUARSKI, IspitniRok.MARTOVSKI, IspitniRok.APRILSKI, IspitniRok.MAJSKI, IspitniRok.JUNSKI, IspitniRok.JULSKI, IspitniRok.SEPTEMBARSKI, IspitniRok.OKTOBARSKI, IspitniRok.NOVEMBARSKI, IspitniRok.DECEMBARSKI);
			} else {
				JSONArray listaIspita = (JSONArray)((JSONObject)student.get("indeks")).get("ispiti");
				for (Object ispit: listaIspita) {
			        JSONObject ispit2 = (JSONObject) ispit;
			        if (ispit2.getString("predmet").equals(izabraniPredmet)) {
			        	 for (IspitniRok rok: listaRokova) {
			        		 if (rok.equals(IspitniRok.valueOf(String.valueOf(ispit2.get("rok"))))) {
			        			 listaRokova.remove(rok);
			        			 break;
			        		 }
			        	 }
			        }
				}
				cbox.getItems().addAll(listaRokova);
			}
		});
		
		ArrayList<String> listaPredmeta = new ArrayList<>();
		
		// izvlacim predmeta za listaPredmeta
		FileReader citac = null;
		JSONArray listaJSON = null;
		try {
			citac = new FileReader("data/predmeti.json");
			listaJSON = new JSONArray(new JSONTokener(citac));
            citac.close();
		} catch (IOException | JSONException b) {
			System.out.println("Greska prilikom rada sa jsonom.");
		}
		
		for (Object predmet: listaJSON) {
			JSONObject predmett = (JSONObject) predmet;
			if (predmett.getString("godinaStudija").equals(((JSONObject)student.get("indeks")).getString("godina studija")) && predmett.getString("smer").equals(((JSONObject)student.get("indeks")).getString("smer"))) {
				listaPredmeta.add(predmett.getString("naziv"));
			}
		}
		for (String predmetNaziv: listaPredmeta) {
			lista.getItems().add(predmetNaziv);
		}

	    
		
		
		Button btn = new Button();
		btn.setText("Prijavi ispit");
		btn.setOnAction(e -> {
			cbox.setVisible(false);
			Ispit noviIspit = new Ispit(((JSONObject)student.get("indeks")).getInt("broj"), 0, 0, LocalDate.now(), false, izabraniPredmet, cbox.getValue());
			((JSONArray)((JSONObject)student.get("indeks")).get("ispiti")).put(new JSONObject(noviIspit));
			PrintWriter upis = null;
			try {
				upis = new PrintWriter("data/korisnici.json");
				upis.write(objekat.toString(4));
				upis.close();
			} catch (IOException | JSONException b) {
				System.out.println("Greska prilikom upisa u json.");
			}
			window.close();
		});
		btn.setTranslateX(300);
		btn.setTranslateY(- 10);
		
        vbox.getChildren().addAll(labela, lista, hbox, btn);
        Scene scena = new Scene(vbox, 400, 400);
        window.setScene(scena);
        window.show();
	}
}
