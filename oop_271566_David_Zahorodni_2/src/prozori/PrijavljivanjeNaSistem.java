package prozori;

import java.io.FileReader;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import enumi.Zvanje;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PrijavljivanjeNaSistem {
		
	static Stage window; //MENI PODSETNIK: buduci da se staticka metoda vezuje samo za klasu i ne trena joj objekat da bi postojala
	//                    tako i sve sto si definisao van nje a koristis u njoj mora takodje postojati i kad nema objekta tj. mora zavisiti samo od klase
	//                    zato window mora da bude static ukoliko ga deklarisemo van staticke metode
	static TextField korisnickoIme;
	static PasswordField lozinka;
	static Label obavestenje;
	public static JSONObject trenutniKorisnik; 
	public static JSONObject korisniciJSON ;
	
	public static void prikaz() {
		window = new Stage();
		window.setTitle("Prijavljivanje na sistem");
		
		VBox vbox = new VBox(10);
		vbox.setPadding(new Insets(20, 10, 10, 10));
		
		//naslov
		Label labela = new Label("Dobro dosli na E-Indeks, molim unesite Vase korisnicko ime i lozinku.");
		labela.setAlignment(Pos.CENTER);
		labela.setId("labela1");
		
		//prvo polje unosa
		korisnickoIme = new TextField();
		korisnickoIme.setPromptText("Unesite Vase korisnicko ime ovde");
		korisnickoIme.setAlignment(Pos.CENTER);
		korisnickoIme.setFocusTraversable(false);
		korisnickoIme.getStyleClass().add("prompt1");
		
		//drugo polje unosa
		lozinka = new PasswordField();
		lozinka.setPromptText("Unesite Vasu lozinku ovde");
		lozinka.setAlignment(Pos.CENTER);
		lozinka.setFocusTraversable(false);
		lozinka.getStyleClass().add("prompt1");

		
		// obavestenje i dugme
		HBox hbox = new HBox();
		obavestenje = new Label("Korisnicko ime ili lozinka nisu tacni, pokusajte ponovo.");
		obavestenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		obavestenje.setVisible(false);
		
		Button btn = new Button("Posalji");
		btn.setOnAction(e -> provera());  
		btn.setId("dugme1");
		btn.setMinWidth(65);
		
		hbox.getChildren().addAll(obavestenje, btn);
		obavestenje.setTranslateX(0);
		btn.setTranslateX(30);
		
		//ubacivanje komponenata u VBox
		vbox.getChildren().addAll(labela, korisnickoIme, lozinka, hbox);
		
		//kreiranje scene
		Scene scena = new Scene(vbox, 425, 165);
		scena.getStylesheets().add("css/login.css");
		window.setScene(scena);
		window.show();
	}
	
	private static void provera() {
		boolean pronadjen = false;
		FileReader citac = null;
		
		
		try {
			citac = new FileReader("data/korisnici.json");
			//vadim objekat iz json fajla
			korisniciJSON = new JSONObject(new JSONTokener(citac));
			// kreiram listu administratora
			JSONArray listaAdministratora = (JSONArray) korisniciJSON.get("administratori");
			for (Object pojedinacno: listaAdministratora) {
//				JSONObject pojedinacno2 = new JSONObject(pojedinacno); PODSETNIK MENI: ovako ne moze da se uradi jer ne moze to da ide kao argument za konstruktor
				JSONObject pojedinacno2 = (JSONObject) pojedinacno; // ne moze new JSONObject(pojedinacno) jer izgleda moze kao argument konstruktora da bude samo tokener, string za objekat, hashmap ili prazan
				if (korisnickoIme.getText().equals(pojedinacno2.getString("korisnickoIme")) && lozinka.getText().equals(pojedinacno2.getString("lozinka")) && !pojedinacno2.getBoolean("obrisan")) {
					pronadjen = true;
					trenutniKorisnik = pojedinacno2;
					administratorskaScena();
				}
			}
			// kreiram listu nastavnika
			JSONArray listaNastavnika = (JSONArray) korisniciJSON.get("nastavnici");
			for (Object nastavnik: listaNastavnika) {
				JSONObject nastavnik2 = (JSONObject) nastavnik;
				if (korisnickoIme.getText().equals(nastavnik2.getString("korisnickoIme")) && lozinka.getText().equals(nastavnik2.getString("lozinka")) && !nastavnik2.getBoolean("obrisan")) {
					trenutniKorisnik = nastavnik2;
					pronadjen = true;
					
					nastavnickaScena();
				}
			}
			// kreiram listu studenata
			JSONArray listaStudenata = (JSONArray) korisniciJSON.get("studenti");
			for (Object studenti: listaStudenata) {
				JSONObject studenti2 = (JSONObject) studenti;
				
				if (korisnickoIme.getText().equals(studenti2.getString("korisnickoIme")) && lozinka.getText().equals(studenti2.getString("lozinka")) && !studenti2.getBoolean("obrisan")){
					pronadjen = true;
					trenutniKorisnik = studenti2;
					studentskaScena();
				}
			}
			// provera da li je doslo do poklapanja
			if (!pronadjen) {
				obavestenje.setVisible(true);
				korisnickoIme.clear();
				lozinka.clear();
				korisnickoIme.requestFocus();
			}
		} catch(IOException | JSONException e) {
				System.out.println("Greska prilikom citanja iz fajla.");
			} finally {
				if (citac != null) {
					try {
						citac.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		

	}
	
	// poziva se ova funkcija u slucaju da je ulogovani korisnik administrator
	// naredne tri funkcije predstavljaju scene za sve tri vrste korisnika
	
	public static void administratorskaScena() {
		window.setTitle("Prijavljeni kao administrator");
		VBox vbox = new VBox(20);
		vbox.setAlignment(Pos.CENTER);
		Label lbl = new Label("Izaberite jednu od sledecih opcija:");
		lbl.getStyleClass().add("labelaTekst");
		
		
		Button btn1 = new Button("Registrovanje korisnika");
		btn1.setMinWidth(200);
		btn1.getStyleClass().add("plavaDugmad");
		btn1.setOnAction(e -> RegistrovanjeKorisnika.prikaz());
		
		Button btn2 = new Button("Brisanje korisnika");
		btn2.setOnAction(e -> BrisanjeKorisnika.prikaz());
		btn2.setMinWidth(200);
		btn2.getStyleClass().add("plavaDugmad");
		
		Button btn3 = new Button("Promena podataka o korisniku");
		btn3.setOnAction(e -> PromenaPodatakaOKorisniku.prikaz());
		btn3.setMinWidth(200);
		btn3.getStyleClass().add("plavaDugmad");
		
		Button btn4 = new Button("Pretraga studenata");
		btn4.setOnAction(e -> PretragaStudenata.prikaz());
		btn4.setMinWidth(200);
		btn4.getStyleClass().add("plavaDugmad");
		
		Button btn5 = new Button("Dodavanje predmeta");
		btn5.setOnAction(e -> DodavanjePredmeta.prikaz()); 
		btn5.setMinWidth(200);
		btn5.getStyleClass().add("plavaDugmad");
		
		vbox.getChildren().addAll(lbl, btn1, btn2, btn3, btn4, btn5);
		Scene scena = new Scene(vbox, 400, 420);
		scena.getStylesheets().add("css/login.css");
		window.setScene(scena);
	}
	
	public static void studentskaScena() {
		window.setTitle("Prijavljeni kao student");
		VBox vbox = new VBox(20);
		vbox.setAlignment(Pos.CENTER);
		
		Label labela = new Label("Izaberite jednu od sledecih opcija:");
		labela.getStyleClass().add("labelaTekst");
		
		HBox hbox = new HBox();
		hbox.setSpacing(10);
		hbox.setAlignment(Pos.CENTER);
		
		Button btn1 = new Button("Prijava ispita");
		btn1.setMinWidth(150);
		btn1.getStyleClass().add("plavaDugmad");
		btn1.setOnAction(e -> PrijavaIspita.prikaz(trenutniKorisnik, korisniciJSON));
		
		Button btn2 = new Button("Ponistavanje ocene");
		btn2.setMinWidth(150);
		btn2.getStyleClass().add("plavaDugmad");
		btn2.setOnAction(e -> PonistavanjeOcene.prikaz(trenutniKorisnik, korisniciJSON));
		
		hbox.getChildren().addAll(btn1, btn2);
		
		Button btn3 = new Button("Pregled indeksa");
		btn3.setMinWidth(200);
		btn3.getStyleClass().add("crvenaDugmad");
		btn3.setOnAction(e -> PregledIndeksa.prikaz(trenutniKorisnik, korisniciJSON));
		
		vbox.getChildren().addAll(labela, hbox, btn3);
		Scene scena = new Scene(vbox, 400, 180);
		scena.getStylesheets().add("css/login.css");
		window.setScene(scena);
		window.show();
	}
	
	public static void nastavnickaScena() {
		window.setTitle("Prijavljeni kao nastavnik");
		VBox vbox = new VBox(20);
		vbox.setAlignment(Pos.CENTER);
		
		Label labela = new Label("Izaberite jednu od sledecih opcija:");
		labela.getStyleClass().add("labelaTekst");
		Label labelaUpozorenje = new Label("Asistent i saradnik ne mogu upisivati ocene.");
		labelaUpozorenje.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
		labelaUpozorenje.setVisible(false);
		
		HBox hbox = new HBox();
		hbox.setSpacing(10);
		hbox.setAlignment(Pos.CENTER);
		
		Button btn1 = new Button("Upis ocena");
		btn1.setMinWidth(150);
		btn1.getStyleClass().add("plavaDugmad");
		btn1.setOnAction(e -> {
			if (Zvanje.valueOf(String.valueOf(trenutniKorisnik.get("zvanje"))).equals(Zvanje.ASISTENT) || Zvanje.valueOf(String.valueOf(trenutniKorisnik.get("zvanje"))).equals(Zvanje.SARADNIK)) {
				labelaUpozorenje.setVisible(true);
			} else {
				UpisOcena.prikaz(korisniciJSON);
			}
		});
		
		Button btn2 = new Button("Pretraga studenata");
		btn2.setMinWidth(150);
		btn2.getStyleClass().add("plavaDugmad");
		btn2.setOnAction(e -> {
			PretragaStudenata.prikaz();
		});
		
		hbox.getChildren().addAll(btn1, btn2);
		
		vbox.getChildren().addAll(labela, hbox, labelaUpozorenje);
		Scene scena = new Scene(vbox, 400, 150);
		scena.getStylesheets().add("css/login.css");
		window.setScene(scena);
		window.show();
	}
}
