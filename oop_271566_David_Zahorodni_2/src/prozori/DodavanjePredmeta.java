package prozori;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import klase.Predmet;

public class DodavanjePredmeta {
		
	public static void prikaz() {
		Stage window = new Stage();
		window.setTitle("Dodavanje predmeta");
		
		// Pravimo JSONObject iz predmeti.json
		
		VBox vbox = new VBox(20);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		
		Label labela = new Label("Kreiranje novog predmeta:");
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(5);
		
		Label labela0 = new Label("Naziv predmeta: ");
		TextField naziv = new TextField();
		
		Label labela1 = new Label("Opis predmeta: ");
		TextField silabus = new TextField();
		
		Label labela2 = new Label("Godina studija: ");
		TextField godinaStudija = new TextField();
		
		Label labela3 = new Label("Godina: ");
		TextField godina = new TextField();
		
		Label labela4 = new Label("Fond predavanja: ");
		TextField fondPredavanja = new TextField();
		
		Label labela5 = new Label("Fond vezbi: ");
		TextField fondVezbi = new TextField();
		
		Label labela6 = new Label("Smer: ");
		TextField smer = new TextField();
		
		Label obavestenje = new Label("ID predmeta se ne moze kreirati dok se ne popune godina i smer predmeta.");
        obavestenje.setVisible(false);
        obavestenje.setId("obavestenje");
        
        Label obavestenje2 = new Label("Sva polja moraju biti popunjena.");
        obavestenje2.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
        obavestenje2.setVisible(false);
        
        Label obavestenje3 = new Label("Predmet sa tim id-em vec postoji, kreirajte id ponovo.");
        obavestenje3.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
        obavestenje3.setVisible(false);
        
        Label obavestenjeOGodini = new Label("Godine studija mogu biti: prva, druga, treca, cetvrta.");
        obavestenjeOGodini.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
        obavestenjeOGodini.setVisible(false);
        
        Label obavestenje4 = new Label("Godina, fond predavanja i fond vezbi moraju da budu celi, pozitivni brojevi.");
        obavestenje4.setStyle("-fx-text-fill: #AB4642; -fx-font-weight: bold");
        obavestenje4.setVisible(false);
		
		TextField idPredmeta = new TextField();
		idPredmeta.setDisable(true);
		Button idButton = new Button("Klikom do id-a predmeta");
		idButton.setOnAction(e -> {
			obavestenje4.setVisible(false);
			obavestenje.setVisible(false);
			if (!godina.getText().matches("[0-9]+") || !fondPredavanja.getText().matches("[0-9]+") || !fondVezbi.getText().matches("[0-9]+")) {
				obavestenje4.setVisible(true);
				godina.clear();
				fondPredavanja.clear();
				fondVezbi.clear();
				godina.requestFocus();
			} else if (godina.getText().equals("") || smer.getText().equals("")) {
				obavestenje.setVisible(true);
				return;
			} else {
				obavestenje.setVisible(false);
				String idString = "";
				Random rnd = new Random();
				char c = (char) (rnd.nextInt(26) + 'a');
				char b = (char) (rnd.nextInt(26) + 'A');

				String br = godina.getText(2, 4);
				
				String sl = smer.getText(); 
				String[] lista = sl.split(" ");
				String poslednjiDeo = "";
				for (String rec: lista) {
					char prvi = rec.charAt(0);
					String drugi = String.valueOf(prvi);
					poslednjiDeo += drugi.toUpperCase();
				}
				
				idString = String.valueOf(c) + String.valueOf(b) + br + poslednjiDeo;
				idPredmeta.setText(idString);
			}
		});
		
		
		grid.add(labela0, 0, 0);
		grid.add(naziv, 1, 0);
		grid.add(labela1,  0,  1);
		grid.add(silabus, 1, 1);
		grid.add(labela2, 0, 2);
		grid.add(godinaStudija, 1, 2);
		grid.add(labela3, 0, 3);
        grid.add(godina, 1, 3);
        grid.add(labela4, 0, 4);
        grid.add(fondPredavanja, 1, 4);
        grid.add(labela5, 0, 5);
        grid.add(fondVezbi, 1, 5);
        grid.add(labela6, 0, 6);
        grid.add(smer, 1, 6);
        grid.add(idButton, 0, 7);
        grid.add(idPredmeta, 1, 7);
        grid.add(obavestenje, 0, 8);
        grid.add(obavestenje2, 0, 9);
        grid.add(obavestenje3, 0, 10);
        grid.add(obavestenjeOGodini, 0, 11);
        
        
        Button dodaj = new Button("Dodaj predmet");
        dodaj.setOnAction(e ->{
        	obavestenje4.setVisible(false);
        	obavestenje.setVisible(false);
        	obavestenje2.setVisible(false);
            obavestenje3.setVisible(false);
            obavestenjeOGodini.setVisible(false);
            
        	if (naziv.getText().equals("") || silabus.getText().equals("") || godinaStudija.getText().equals("") || godina.getText().equals("") || fondPredavanja.getText().equals("") || fondVezbi.getText().equals("") || smer.getText().equals("") || idPredmeta.getText().equals("")) {
        		obavestenje2.setVisible(true);
        	} else if (!godinaStudija.getText().equals("prva") && !godinaStudija.getText().equals("druga") && !godinaStudija.getText().equals("treca") && !godinaStudija.getText().equals("cetvrta")) {
        		obavestenjeOGodini.setVisible(true);
        	} else if (!godina.getText().matches("[0-9]+") || !fondPredavanja.getText().matches("[0-9]+") || !fondVezbi.getText().matches("[0-9]+")) {
        		obavestenje4.setVisible(true);
        	}
        	
        	else {
        		JSONObject predmet = new JSONObject(new Predmet(idPredmeta.getText(), naziv.getText(), silabus.getText(), smer.getText(), Integer.parseInt(godina.getText()), godinaStudija.getText(), Integer.parseInt(fondPredavanja.getText()), Integer.parseInt(fondVezbi.getText()))); 
        		File fajl = new File("data/predmeti.json");
            	if (!fajl.exists()) {
            		JSONArray listaPredmeta = new JSONArray();
            		listaPredmeta.put(predmet);
            		PrintWriter upis = null;
            		try {
            			upis = new PrintWriter("data/predmeti.json");
            			upis.write(listaPredmeta.toString(4));
            			upis.close();
            		} catch (IOException | JSONException b) {
            			System.out.println("Greska prilikom upisa u json fajl.");
            		}
            		window.close();
            	} else {
            		FileReader citac = null;
            		JSONArray procitaniPredmeti = null;
            		boolean stanje = false;
            		try {
            			citac = new FileReader("data/predmeti.json");
            			procitaniPredmeti = new JSONArray(new JSONTokener(citac));
            			citac.close();
            		} catch(IOException | JSONException b) {
                        System.out.println("Greska prilikom citanja iz json fajla.");
            		}
            		for (Object predmett: procitaniPredmeti) {
            			JSONObject predmettt = (JSONObject) predmett;
            			if (predmettt.getString("id").equals(idPredmeta.getText())) {
                            
                            stanje = true;
                        }
            		}
            		if (stanje) {
            			obavestenje3.setVisible(true);
            		} else {
            			PrintWriter upis = null;
                		try {
                			upis = new PrintWriter("data/predmeti.json");
                			procitaniPredmeti.put(predmet);
                			upis.write(procitaniPredmeti.toString(4));
                			upis.close();
                		} catch (IOException | JSONException b) {
                			System.out.println("Greska prilikom upisa u json fajl.");
                		}
                		window.close();
            		}
            		
            	}
        	}
        	

        });
        dodaj.setTranslateX(450);
        
        vbox.getChildren().addAll(labela, grid, dodaj, obavestenje4);
		
        Scene scena = new Scene(vbox, 580, 500);
        scena.getStylesheets().add("css/dodavanjePredmeta.css");
        window.setScene(scena);
        window.show();
        
	}


}
