package prozori;

import org.json.JSONObject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PregledIndeksa {
	public static void prikaz(JSONObject student, JSONObject objekat) {
		Stage window = new Stage();
		window.setTitle("Pregled indeksa");
		
		VBox vbox = new VBox(10);
		vbox.setPadding(new Insets(10, 10, 10, 10));
		GridPane gridStudenta = new GridPane();
		gridStudenta.setVgap(6);
		
		Label label1 = new Label("Ime studenta: ");
		TextField ime = new TextField();
		ime.setText(student.getString("ime"));
		ime.setEditable(false);
		
		Label label2 = new Label("Prezime studenta: ");
		TextField prezime = new TextField();
		prezime.setText(student.getString("prezime"));
		prezime.setEditable(false);
		
		Label label3 = new Label("Korisnicko ime studenta: ");
		TextField korisnickoIme = new TextField();
		korisnickoIme.setText(student.getString("korisnickoIme"));
		korisnickoIme.setEditable(false);
		
		Label label4 = new Label("Lozinku studenta: ");
		TextField lozinka = new TextField();
		lozinka.setText(student.getString("lozinka"));
		lozinka.setEditable(false);
		
		Label label5 = new Label("Email studenta: ");
		TextField email = new TextField();
	    email.setText(student.getString("email"));
	    email.setEditable(false);
		
		Label label6 = new Label("Broj telefona studenta: ");
		TextField brojTelefona = new TextField();
		brojTelefona.setText(String.valueOf(student.getLong("brojTelefona")));
		brojTelefona.setEditable(false);
		
		Label label7 = new Label("Indeksa studenta: ");
		
		// za indeks
		Label label8 = new Label("Broj indeksa studenta: ");
		TextField indeksBroj = new TextField();
		indeksBroj.setText(String.valueOf(student.getJSONObject("indeks").getInt("broj")));
		indeksBroj.setEditable(false);
		
		Label label9 = new Label("Univerzitet studenta: ");
		TextField indeksUniverzitet = new TextField();
		indeksUniverzitet.setText(student.getJSONObject("indeks").getString("univerzitet"));
		indeksUniverzitet.setEditable(false);
		
		Label label10 = new Label("Fakultet studenta: ");
		TextField indeksFakultet = new TextField();
	    indeksFakultet.setText(student.getJSONObject("indeks").getString("fakultet"));
	    indeksFakultet.setEditable(false);
		
		Label label11 = new Label("Smer studenta: ");
		TextField indeksSmer = new TextField();
		indeksSmer.setText(student.getJSONObject("indeks").getString("smer"));
		indeksSmer.setEditable(false);
		
		Label label12 = new Label("Godinu upisa studenta: ");
		TextField indeksGodinaUpisa = new TextField();
		indeksGodinaUpisa.setText(String.valueOf(student.getJSONObject("indeks").getInt("godina upisa")));
		indeksGodinaUpisa.setEditable(false);
		
		Label label13 = new Label("Godinu studija studenta: ");
		TextField indeksGodinaStudija = new TextField();
		indeksGodinaStudija.setText(student.getJSONObject("indeks").getString("godina studija"));
	    indeksGodinaStudija.setEditable(false);
	  
		
		
		gridStudenta.add(label1, 0, 0);
		gridStudenta.add(ime, 1, 0);
		gridStudenta.add(label2, 0, 1);
		gridStudenta.add(prezime, 1, 1);
		gridStudenta.add(label3, 0, 2);
		gridStudenta.add(korisnickoIme, 1, 2);
		gridStudenta.add(label4, 0, 3);
		gridStudenta.add(lozinka, 1, 3);
		gridStudenta.add(label5, 0, 4); 
		gridStudenta.add(email, 1, 4);
		gridStudenta.add(label6, 0, 5);
		gridStudenta.add(brojTelefona, 1, 5);
		gridStudenta.add(label7, 0, 6);

		gridStudenta.add(label8, 0, 7);
		gridStudenta.add(indeksBroj, 1, 7);
		gridStudenta.add(label9, 0, 8);
		gridStudenta.add(indeksUniverzitet, 1, 8);
		gridStudenta.add(label10, 0, 9);
		gridStudenta.add(indeksFakultet, 1, 9);
		gridStudenta.add(label11, 0, 10);
		gridStudenta.add(indeksSmer, 1, 10);
		gridStudenta.add(label12, 0, 11);
		gridStudenta.add(indeksGodinaUpisa, 1, 11);
		gridStudenta.add(label13, 0, 12);
		gridStudenta.add(indeksGodinaStudija, 1, 12);
		
		ObservableList<String> lista1 = FXCollections.observableArrayList();
		ObservableList<String> lista2 = FXCollections.observableArrayList();
		
		// izvlacim ispite i sortiram ih u dve liste prema tome da li su polozeni ili su ponisteni
		for (Object ispitJSON: student.getJSONObject("indeks").getJSONArray("ispiti")) {
			JSONObject ispit = (JSONObject) ispitJSON;
			if (!ispit.getBoolean("ponisten")) {
				String stringIspita = "Ispit: " + ispit.getString("predmet") + ", ocena: " + String.valueOf(ispit.getInt("ocena")) + ", rok: " + String.valueOf(ispit.get("rok"));
				lista1.add(stringIspita);
			} else {
				String stringIspita = "Ispit: " + ispit.getString("predmet") + ", ocena: " + String.valueOf(ispit.getInt("ocena")) + ", rok: " + String.valueOf(ispit.get("rok"));
				lista2.add(stringIspita);
			}
		}
		
		VBox vboxLista1 = new VBox(5);
		Label labela1 = new Label("Pregled polozenih ispita:");
		ListView<String> listaPolozenihIspita = new ListView<>();
		listaPolozenihIspita.getItems().addAll(lista1);

		listaPolozenihIspita.setMaxWidth(250);
		vboxLista1.getChildren().addAll(labela1, listaPolozenihIspita);
		
		
		VBox vboxLista2 = new VBox(5);
		Label labela2 = new Label("Pregled ponistenih ispita:");
		ListView<String> listaPonistenihIspita = new ListView<>();

        listaPonistenihIspita.setMaxWidth(250);
		listaPonistenihIspita.getItems().addAll(lista2);
		
		vboxLista2.getChildren().addAll(labela2, listaPonistenihIspita);
		
		VBox vboxListe = new VBox(20);
		vboxListe.getChildren().addAll(vboxLista1, vboxLista2);
		
		Button btn1 = new Button("Prijava ispita");
		btn1.setMinWidth(150);
		btn1.getStyleClass().add("plavaDugmad");
		btn1.setOnAction(e -> {
			PrijavaIspita.prikaz(student, objekat);
			window.close();
		});
		
		
		Button btn2 = new Button("Ponistavanje ocene");
		btn2.setMinWidth(150);
		btn2.getStyleClass().add("plavaDugmad");
		btn2.setOnAction(e -> {
			PonistavanjeOcene.prikaz(student, objekat);
			window.close();
		});
		
		HBox hboxp = new HBox();
		hboxp.setSpacing(20);
		hboxp.getChildren().addAll(gridStudenta, vboxListe);
		vbox.getChildren().addAll(hboxp, btn1, btn2);
		Scene scena = new Scene(vbox, 650, 550);
		scena.getStylesheets().add("css/pregledIndeksa.css");
		window.setScene(scena);
		window.show();
	}
}
